<?php 
define("ROOT_FOLDER", __DIR__ . "/"); 
define("GAME_FOLDER", __DIR__ . "/game/");
define("GAME_CLASSES_FOLDER", __DIR__ . "/game/php/classes/");
define("GAME_CLASSES_ITEMS_FOLDER", __DIR__ . "/game/php/classes/items/");
define("GAME_IMAGES_FOLDER", __DIR__ . "/game/img/");
define("ADMIN_FOLDER", __DIR__ . "/editor/");
?>