<?php
require_once '../config.php';
require_once GAME_CLASSES_FOLDER . '/sql.php';
require_once GAME_CLASSES_FOLDER . 'player.php';
require_once GAME_CLASSES_FOLDER . 'monster.php';

session_start();
// Check if logged in
if(!isset($_SESSION['player'])){
    header ('Location: ../index.php');
} else if ($_SESSION['player']->currentHitpoints == 0){
    header ('Location: profile.php');
    die();
}
else 
{
    $player = $_SESSION['player'];
}
// Check if enemy exists
if(!isset($_SESSION['enemy']) || $_SESSION['enemy']->currentHitpoints == 0){
    // If not, generate one
    $result = SQL::get_monster_by_zone($_SESSION['player']->currentZone->id);
    $_SESSION['enemy'] = new Monster(($result['result']));
}
?>
<!DOCTYPE html>
<html lang="lt">
<head>
    <meta charset="UTF-8">
    <title>The Void - Battle Stage</title>
    <link href="css/battle_layout.css" rel="stylesheet" type="text/css">
    <link href="css/battle_skin.css" rel="stylesheet" type="text/css">
    <script src="js/battle_action.js"></script>
</head>
<body style="background-image:url(<?php echo $player->currentZone->imagePath;?>);" 
    onload="setHpBars(<?php echo $_SESSION['enemy']->currentHitpoints . ',' . $_SESSION['enemy']->hitpoints . ',' . $_SESSION['player']->currentHitpoints . ',' . $_SESSION['player']->hitpoints; ?>)">
    <div id="content">
        <div id="top">
            <?php
                // Player column
                require_once 'incl/battle_player_column.php';
                // Monster column
                require_once 'incl/battle_monster_column.php';
            ?>
        </div>
        <div id="bottom">
            <div id="battleButtonsColumn">
                <div id="battleActionButtons">
                    <button type="button" onclick="action('attack')">Attack</button>
                    <button type="button" onclick="action('defend')">Defend</button>
                    <button type="button" onclick="action('run')">Run Away</button>
                </div>
                <div id="battleResultButtons">
                    <button type="button" onclick="navigateTo('profile')">Back to Profile</button>
                    <button type="button" onclick="navigateTo('next')">Next</button>
                </div>
            </div>
            <div id="battleLogColumn">
                <div id="battleLogContainer">
                    <p id="battle_log"></p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>