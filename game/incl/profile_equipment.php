<?php
$player = $_SESSION['player'];
?>
<div class="col">
    <h1 class="header">Equipment</h1>
    <div id="equipmentTable">
        <div class="row">
            <div class="equipment">
                <img class="equipmentImage inventorySlotI" src="<?php echo $player->headArmour->imagePath ?? "img/headArmour/default.png" ?>" onclick="showMenu(8,<?php echo $player->headArmour->id ?>)">
                <div id="myDropdown8" class="dropdown-content">
                    <button type="button" onclick="unequip('HeadArmour', <?php echo $player->headArmour->id?>)">Unequip</button>
                    <button type="button" onclick="showInfo('HeadArmour')">Information</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="equipment">
                <img class="equipmentImage inventorySlotI" src="<?php echo $player->weapon->imagePath ?? "img/weapon/default.png" ?>" onclick="showMenu(9,<?php echo $player->weapon->id ?>)"/>
                <div id="myDropdown9" class="dropdown-content">
                    <button type="button" onclick="unequip('Weapon', <?php echo $player->weapon->id?>)">Unequip</button>
                    <button type="button" onclick="showInfo('Weapon')">Information</button>
                </div>
            </div>
            <div class="equipment">
                <img class="equipmentImage inventorySlotI" src="<?php echo $player->torsoArmour->imagePath ?? "img/torsoArmour/default.png" ?>" onclick="showMenu(10,<?php echo $player->torsoArmour->id ?>)"/>
                <div id="myDropdown10" class="dropdown-content">
                    <button type="button" onclick="unequip('TorsoArmour', <?php echo $player->torsoArmour->id?>)">Unequip</button>
                    <button type="button" onclick="showInfo('TorsoArmour')">Information</button>
                </div>
            </div>
            <div class="equipment">
                <img class="equipmentImage inventorySlotI" src="<?php echo $player->shield->imagePath ?? "img/shield/default.png" ?>" onclick="showMenu(11,<?php echo $player->weapon->id ?>)"/>   
                <div id="myDropdown11" class="dropdown-content">
                    <button type="button" onclick="unequip('Shield', <?php echo $player->shield->id ?>)">Unequip</button>
                    <button type="button" onclick="showInfo('Shield')">Information</button>
                </div>
            </div>     
        </div>
        <div class="row">
            <div class="equipment">
                <img class="equipmentImage inventorySlotI" src="<?php echo $player->legsArmour->imagePath ?? "img/legsArmour/default.png" ?>" onclick="showMenu(12,<?php echo $player->weapon->id ?>)"/>
                <div id="myDropdown12" class="dropdown-content">
                    <button type="button" onclick="unequip('LegsArmour', <?php echo $player->legsArmour->id?>)">Unequip</button>
                    <button type="button" onclick="showInfo('LegsArmour')">Information</button>
                </div>
            </div>
        </div>
    </div>
</div>