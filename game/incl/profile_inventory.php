<?php
$player = $_SESSION['player'];
?>

<div class="col">
    <h1 class="header">Inventory</h1>
    <div id="profileInventory">
        <div class="row">
            <div id="slot0" class="inventorySlot">
                <img src="<?php echo $player->inventory["slot0"]->imagePath ?? 'img/square.png' ?>" class="inventorySlotI"  onclick="showMenu(0,<?php echo $player->inventory['slot0']->id ?>)">
                <div id="myDropdown0" class="dropdown-content">
                    <button type="button" onclick="equip('slot0')">Equip</button>
                    <button type="button" onclick="showInfo('slot0')">Information</button>
                    <button type="button" onclick="drop('slot0')">Drop</button>
                </div>
            </div>
            <div id="slot1" class="inventorySlot">
                <img src="<?php echo $player->inventory["slot1"]->imagePath ?? 'img/square.png' ?>" class="inventorySlotI"  onclick="showMenu(1, <?php echo $player->inventory['slot1']->id ?>)">
                <div id="myDropdown1" class="dropdown-content">
                    <button type="button" onclick="equip('slot1')">Equip</button>
                    <button type="button" onclick="showInfo('slot1')">Information</button>
                    <button type="button" onclick="drop('slot1')">Drop</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="slot2" class="inventorySlot">
                <img src="<?php echo $player->inventory["slot2"]->imagePath ?? 'img/square.png' ?>" class="inventorySlotI"  onclick="showMenu(2, <?php echo $player->inventory['slot2']->id ?>)">
                <div id="myDropdown2" class="dropdown-content">
                    <button type="button" onclick="equip('slot2')">Equip</button>
                    <button type="button" onclick="showInfo('slot2')">Information</button>
                    <button type="button" onclick="drop('slot2')">Drop</button>
                </div>
            </div>
            <div id="slot3" class="inventorySlot">
                <img src="<?php echo $player->inventory["slot3"]->imagePath ?? 'img/square.png' ?>" class="inventorySlotI"  onclick="showMenu(3, <?php echo $player->inventory['slot3']->id ?>)">
                <div id="myDropdown3" class="dropdown-content">
                    <button type="button" onclick="equip('slot3')">Equip</button>
                    <button type="button" onclick="showInfo('slot3')">Information</button>
                    <button type="button" onclick="drop('slot3')">Drop</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="slot4" class="inventorySlot">
                <img src="<?php echo $player->inventory["slot4"]->imagePath ?? 'img/square.png' ?>" class="inventorySlotI"  onclick="showMenu(4, <?php echo $player->inventory['slot4']->id ?>)">
                <div id="myDropdown4" class="dropdown-content">
                    <button type="button" onclick="equip('slot4')">Equip</button>
                    <button type="button" onclick="showInfo('slot4')">Information</button>
                    <button type="button" onclick="drop('slot4')">Drop</button>
                </div>
            </div>
            <div id="slot5" class="inventorySlot">
                <img src="<?php echo $player->inventory["slot5"]->imagePath ?? 'img/square.png' ?>" class="inventorySlotI"  onclick="showMenu(5, <?php echo $player->inventory['slot5']->id ?>)">
                <div id="myDropdown5" class="dropdown-content">
                    <button type="button" onclick="equip('slot5')">Equip</button>
                    <button type="button" onclick="showInfo('slot5')">Information</button>
                    <button type="button" onclick="drop('slot5')">Drop</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="slot6" class="inventorySlot">
                <img src="<?php echo $player->inventory["slot6"]->imagePath ?? 'img/square.png' ?>" class="inventorySlotI"  onclick="showMenu(6, <?php echo $player->inventory['slot6']->id ?>)">
                <div id="myDropdown6" class="dropdown-content">
                    <button type="button" onclick="equip('slot6')">Equip</button>
                    <button type="button" onclick="showInfo('slot6')">Information</button>
                    <button type="button" onclick="drop('slot6')">Drop</button>
                </div>
            </div>
            <div id="slot7" class="inventorySlot">
                <img src="<?php echo $player->inventory["slot7"]->imagePath ?? 'img/square.png' ?>" class="inventorySlotI"  onclick="showMenu(7, <?php echo $player->inventory['slot7']->id ?>)">
                <div id="myDropdown7" class="dropdown-content">
                    <button type="button" onclick="equip('slot7')">Equip</button>
                    <button type="button" onclick="showInfo('slot7')">Information</button>
                    <button type="button" onclick="drop('slot7')">Drop</button>
                </div>
            </div>
        </div>
    </div>
</div>