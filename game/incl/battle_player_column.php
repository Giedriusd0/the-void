<?php $player = $_SESSION['player']; ?>
<div id="battlePlayerColumn">
    <div class="characterContainer">
        <h2 class="header"><?php echo $player->name ?></h2>
        <div class="imageContainer">
            <img src="<?php echo $player->imagePath ?>">
            <img id="playerSplashDammageImage" class="splashDamage" src="img/splatter.png">
            <p id="playerSplashDamageAmmount" class="splashDamageAmmount"></p>
        </div>
        <div class="healthPoints">
            <span id="playerHealthBar" class="healthPointsBar"></span>
            <span id="playerCurrentHealth"><?php echo $player->currentHitpoints?><?php echo " / $player->hitpoints"?></span>
        </div>
    </div>
</div>