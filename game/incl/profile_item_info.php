<div id="overlayInfo" onclick="showInfo('hide')">
    <!--  EQUIPMENT  -->
    <div id="equipHeadInfo" class="itemInfoContainer" onclick="event.stopPropagation();">
        <!-- Name -->
        <h3><?php echo $_SESSION['player']->headArmour->name ?? null; ?></h3>
        <!-- Image -->
        <img src="<?php echo $_SESSION['player']->headArmour->imagePath ?? null; ?>" >
        <!-- Stats -->
        <div class="infoStats">
            <!-- Min Defense -->
            <p>Minimal defense: <?php echo $_SESSION['player']->headArmour->minDefense ?? null; ?></p>
            <!-- Max Defense -->
            <p>Maximum defense: <?php echo $_SESSION['player']->headArmour->maxDefense ?? null; ?></p>
        </div>
        <!-- Description -->
        <p>Description:</p>
        <p><?php echo $_SESSION['player']->headArmour->description ?? null; ?></p>
        <button type="button" onclick="showInfo('hide')">Close</button>
    </div>
    <div id="equipTorsoInfo" class="itemInfoContainer" onclick="event.stopPropagation();">
        <!-- Name -->
        <h3><?php echo $_SESSION['player']->torsoArmour->name ?? null; ?></h3>
        <!-- Image -->
        <img src="<?php echo $_SESSION['player']->torsoArmour->imagePath ?? null; ?>">
        <!-- Stats -->
        <div class="infoStats">
            <!-- Min Defense -->
            <p>Minimal defense: <?php echo $_SESSION['player']->torsoArmour->minDefense ?? null; ?></p>
            <!-- Max Defense -->
            <p>Maximum defense: <?php echo $_SESSION['player']->torsoArmour->maxDefense ?? null; ?></p>
        </div>
        <!-- Description -->
        <p>Description:</p>
        <p><?php echo $_SESSION['player']->torsoArmour->description ?? null; ?></p>
        <button type="button" onclick="showInfo('hide')">Close</button>
    </div>
    <div id="equipLegsInfo" class="itemInfoContainer" onclick="event.stopPropagation();">
        <!-- Name -->
        <h3><?php echo $_SESSION['player']->legsArmour->name ?? null; ?></h3>
        <!-- Image -->
        <img src="<?php echo $_SESSION['player']->legsArmour->imagePath ?? null; ?>" >
        <!-- Stats -->
        <div class="infoStats">
            <!-- Min Defense -->
            <p>Minimal defense: <?php echo $_SESSION['player']->legsArmour->minDefense ?? null; ?></p>
            <!-- Max Defense -->
            <p>Maximum defense: <?php echo $_SESSION['player']->legsArmour->maxDefense ?? null; ?></p>
        </div>
        <!-- Description -->
        <p>Description:</p>
        <p><?php echo $_SESSION['player']->legsArmour->description ?? null; ?></p>
        <button type="button" onclick="showInfo('hide')">Close</button>
    </div>
    <div id="equipWeaponInfo" class="itemInfoContainer" onclick="event.stopPropagation();">
        <!-- Name -->
        <h3><?php echo $_SESSION['player']->weapon->name ?? null; ?></h3>
        <!-- Image -->
        <img src="<?php echo $_SESSION['player']->weapon->imagePath ?? null; ?>" >
        <!-- Stats -->
        <div class="infoStats">
            <!-- Min Damage -->
            <p>Minimal damage: <?php echo $_SESSION['player']->weapon->minDamage ?? null; ?></p>
            <!-- Max Damage -->
            <p>Maximum damage: <?php echo $_SESSION['player']->weapon->maxDamage ?? null; ?></p>
        </div>
        <!-- Description -->
        <p>Description:</p>
        <p><?php echo $_SESSION['player']->weapon->description ?? null; ?></p>
        <button type="button" onclick="showInfo('hide')">Close</button>
    </div>
    <div id="equipShieldInfo" class="itemInfoContainer" onclick="event.stopPropagation();">
        <!-- Name -->
        <h3><?php echo $_SESSION['player']->shield->name ?? null; ?></h3>
        <!-- Image -->
        <img src="<?php echo $_SESSION['player']->shield->imagePath ?? null; ?>" >
        <!-- Stats -->
        <div class="infoStats">
            <!-- Min Defense -->
            <p>Minimal defense: <?php echo $_SESSION['player']->shield->minDefense ?? null; ?></p>
            <!-- Max Defense -->
            <p>Maximum defense: <?php echo $_SESSION['player']->shield->maxDefense ?? null; ?></p>
        </div>
        <!-- Description -->
        <p>Description:</p>
        <p><?php echo $_SESSION['player']->shield->description ?? null; ?></p>
        <button type="button" onclick="showInfo('hide')">Close</button>
    </div>
    <!--  INVENTORY  -->
    <div id="slot0Info" class="itemInfoContainer" onclick="event.stopPropagation();">
        <!-- Name -->
        <h3><?php echo $_SESSION['player']->inventory['slot0']->name ?? null; ?></h3>
        <!-- Image -->
        <img src="<?php echo $_SESSION['player']->inventory['slot0']->imagePath ?? null; ?>" >
        <!-- Stats -->
        <div class="infoStats">
            <!-- Min Defense -->
            <p>Minimal defense: <?php echo $_SESSION['player']->inventory['slot0']->minDefense ?? $_SESSION['player']->inventory['slot0']->minDamage ?? null; ?></p>
            <!-- Max Defense -->
            <p>Maximum defense: <?php echo $_SESSION['player']->inventory['slot0']->maxDefense ?? $_SESSION['player']->inventory['slot0']->maxDamage ?? null; ?></p>
        </div>
        <!-- Description -->
        <p>Description:</p>
        <p><?php echo $_SESSION['player']->inventory['slot0']->description ?? null; ?></p>
        <button type="button" onclick="showInfo('hide')">Close</button>
    </div>
    <div id="slot1Info" class="itemInfoContainer" onclick="event.stopPropagation();">
        <!-- Name -->
        <h3><?php echo $_SESSION['player']->inventory['slot1']->name ?? null; ?></h3>
        <!-- Image -->
        <img src="<?php echo $_SESSION['player']->inventory['slot1']->imagePath ?? null; ?>" >
        <!-- Stats -->
        <div class="infoStats">
            <!-- Min Defense -->
            <p>Minimal defense: <?php echo $_SESSION['player']->inventory['slot1']->minDefense ?? $_SESSION['player']->inventory['slot1']->minDamage ?? null; ?></p>
            <!-- Max Defense -->
            <p>Maximum defense: <?php echo $_SESSION['player']->inventory['slot1']->maxDefense ?? $_SESSION['player']->inventory['slot1']->maxDamage ?? null; ?></p>
        </div>
        <!-- Description -->
        <p>Description:</p>
        <p><?php echo $_SESSION['player']->inventory['slot1']->description ?? null; ?></p>
        <button type="button" onclick="showInfo('hide')">Close</button>
    </div>
    <div id="slot2Info" class="itemInfoContainer" onclick="event.stopPropagation();">
        <!-- Name -->
        <h3><?php echo $_SESSION['player']->inventory['slot2']->name ?? null; ?></h3>
        <!-- Image -->
        <img src="<?php echo $_SESSION['player']->inventory['slot2']->imagePath ?? null; ?>" >
        <!-- Stats -->
        <div class="infoStats">
            <!-- Min Defense -->
            <p>Minimal defense: <?php echo $_SESSION['player']->inventory['slot2']->minDefense ?? $_SESSION['player']->inventory['slot2']->minDamage ?? null; ?></p>
            <!-- Max Defense -->
            <p>Maximum defense: <?php echo $_SESSION['player']->inventory['slot2']->maxDefense ?? $_SESSION['player']->inventory['slot2']->maxDamage ?? null; ?></p>
        </div>
        <!-- Description -->
        <p>Description:</p>
        <p><?php echo $_SESSION['player']->inventory['slot2']->description ?? null; ?></p>
        <button type="button" onclick="showInfo('hide')">Close</button>
    </div>
    <div id="slot3Info" class="itemInfoContainer" onclick="event.stopPropagation();">
        <!-- Name -->
        <h3><?php echo $_SESSION['player']->inventory['slot3']->name ?? null; ?></h3>
        <!-- Image -->
        <img src="<?php echo $_SESSION['player']->inventory['slot3']->imagePath ?? null; ?>" >
        <!-- Stats -->
        <div class="infoStats">
            <!-- Min Defense -->
            <p>Minimal defense: <?php echo $_SESSION['player']->inventory['slot3']->minDefense ?? $_SESSION['player']->inventory['slot3']->minDamage ?? null; ?></p>
            <!-- Max Defense -->
            <p>Maximum defense: <?php echo $_SESSION['player']->inventory['slot3']->maxDefense ?? $_SESSION['player']->inventory['slot3']->maxDamage ?? null; ?></p>
        </div>
        <!-- Description -->
        <p>Description:</p>
        <p><?php echo $_SESSION['player']->inventory['slot3']->description ?? null; ?></p>
        <button type="button" onclick="showInfo('hide')">Close</button>
    </div>
    <div id="slot4Info" class="itemInfoContainer" onclick="event.stopPropagation();">
        <!-- Name -->
        <h3><?php echo $_SESSION['player']->inventory['slot4']->name ?? null; ?></h3>
        <!-- Image -->
        <img src="<?php echo $_SESSION['player']->inventory['slot4']->imagePath ?? null; ?>" >
        <!-- Stats -->
        <div class="infoStats">
            <!-- Min Defense -->
            <p>Minimal defense: <?php echo $_SESSION['player']->inventory['slot4']->minDefense ?? $_SESSION['player']->inventory['slot4']->minDamage ?? null; ?></p>
            <!-- Max Defense -->
            <p>Maximum defense: <?php echo $_SESSION['player']->inventory['slot4']->maxDefense ?? $_SESSION['player']->inventory['slot4']->maxDamage ?? null; ?></p>
        </div>
        <!-- Description -->
        <p>Description:</p>
        <p><?php echo $_SESSION['player']->inventory['slot4']->description ?? null; ?></p>
        <button type="button" onclick="showInfo('hide')">Close</button>
    </div>
    <div id="slot5Info" class="itemInfoContainer" onclick="event.stopPropagation();">
        <!-- Name -->
        <h3><?php echo $_SESSION['player']->inventory['slot5']->name ?? null; ?></h3>
        <!-- Image -->
        <img src="<?php echo $_SESSION['player']->inventory['slot5']->imagePath ?? null; ?>" >
        <!-- Stats -->
        <div class="infoStats">
            <!-- Min Defense -->
            <p>Minimal defense: <?php echo $_SESSION['player']->inventory['slot5']->minDefense ?? $_SESSION['player']->inventory['slot5']->minDamage ?? null; ?></p>
            <!-- Max Defense -->
            <p>Maximum defense: <?php echo $_SESSION['player']->inventory['slot5']->maxDefense ?? $_SESSION['player']->inventory['slot5']->maxDamage ?? null; ?></p>
        </div>
        <!-- Description -->
        <p>Description:</p>
        <p><?php echo $_SESSION['player']->inventory['slot5']->description ?? null; ?></p>
        <button type="button" onclick="showInfo('hide')">Close</button>
    </div>
    <div id="slot6Info" class="itemInfoContainer" onclick="event.stopPropagation();">
        <!-- Name -->
        <h3><?php echo $_SESSION['player']->inventory['slot6']->name ?? null; ?></h3>
        <!-- Image -->
        <img src="<?php echo $_SESSION['player']->inventory['slot6']->imagePath ?? null; ?>" >
        <!-- Stats -->
        <div class="infoStats">
            <!-- Min Defense -->
            <p>Minimal defense: <?php echo $_SESSION['player']->inventory['slot6']->minDefense ?? $_SESSION['player']->inventory['slot6']->minDamage ?? null; ?></p>
            <!-- Max Defense -->
            <p>Maximum defense: <?php echo $_SESSION['player']->inventory['slot6']->maxDefense ?? $_SESSION['player']->inventory['slot6']->maxDamage ?? null; ?></p>
        </div>
        <!-- Description -->
        <p>Description:</p>
        <p><?php echo $_SESSION['player']->inventory['slot6']->description ?? null; ?></p>
        <button type="button" onclick="showInfo('hide')">Close</button>
    </div>
    <div id="slot7Info" class="itemInfoContainer" onclick="event.stopPropagation();">
        <!-- Name -->
        <h3><?php echo $_SESSION['player']->inventory['slot7']->name ?? null; ?></h3>
        <!-- Image -->
        <img src="<?php echo $_SESSION['player']->inventory['slot7']->imagePath ?? null; ?>" >
        <!-- Stats -->
        <div class="infoStats">
            <!-- Min Defense -->
            <p>Minimal defense: <?php echo $_SESSION['player']->inventory['slot7']->minDefense ?? $_SESSION['player']->inventory['slot7']->minDamage ?? null; ?></p>
            <!-- Max Defense -->
            <p>Maximum defense: <?php echo $_SESSION['player']->inventory['slot7']->maxDefense ?? $_SESSION['player']->inventory['slot7']->maxDamage ?? null; ?></p>
        </div>
        <!-- Description -->
        <p>Description:</p>
        <p><?php echo $_SESSION['player']->inventory['slot7']->description ?? null; ?></p>
        <button type="button" onclick="showInfo('hide')">Close</button>
    </div>
</div>