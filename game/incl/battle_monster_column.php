<?php $enemy = $_SESSION['enemy']; ?>
<div id="battleMonsterColumn">
    <div id="monsterImageContainer" class="characterContainer">
        <h2 class="header"><?php echo $enemy->name ?></h2>
        <div class="imageContainer">
            <img src="<?php echo $enemy->imagePath ?>">
            <img id="monsterSplashImage" class="splashDamage" src="img/splatter.png">
            <p id="monsterSplashAmmount" class="splashDamageAmmount"></p>
            <p id="monsterDescription"><?php echo $enemy->description; ?></p>
        </div>
        <div class="healthPoints">
            <span id="enemyHealthBar" class="healthPointsBar"></span>
            <span id="enemyCurrentHealth"><?php echo $enemy->currentHitpoints?><?php echo " / $enemy->hitpoints"?></span>
        </div>
    </div>
</div>