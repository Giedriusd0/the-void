<?php
$player = $_SESSION['player'];
?>
<div class="col">
    <h1 class="header"><?php echo "$player->name ($player->level level)" ?></h1>
    <h1 class="subHeader"><?php echo "$player->experienceNext experience to next level."?></h1>
        <img id="profileAvatar" src="<?php echo $player->imagePath?>">
    <div id="profileStats">
        <p>
            Remaining Points:<span id="charFreePoints"><?php echo $player->remainingPoints ?></span>
        </p>
        <p>
            <span class="distributePoints">
                <button type="button" class="pointsButton" onclick="distribute('att','+')">+</button>
                <span id="pointsDistributedAtt">0</span>
                <button type="button" class="pointsButton" onclick="distribute('att','-')">-</button>
            </span>
            Attack:<span id="charAttack"><?php echo $player->attack?></span>
        </p>
        <p>
            <span class="distributePoints">
                <button type="button" class="pointsButton" onclick="distribute('str','+')">+</button>
                <span id="pointsDistributedStr">0</span>
                <button type="button" class="pointsButton" onclick="distribute('str','-')">-</button>
            </span>
            Strength:<span id="charStrength"><?php echo $player->strength?></span>
        </p>
        <p>
            <span class="distributePoints">
                <button type="button" class="pointsButton" onclick="distribute('h','+')">+</button>
                <span id="pointsDistributedH">0</span>
                <button type="button" class="pointsButton" onclick="distribute('h','-')">-</button>
            </span>
            Vitality:<span id="charHealth"><?php echo $player->vitality?></span>
        </p>
        <p>
            <span class="distributePoints">
                <button type="button" class="pointsButton" onclick="distribute('def','+')">+</button>
                <span id="pointsDistributedDef">0</span>
                <button type="button" class="pointsButton" onclick="distribute('def','-')">-</button>
            </span>
            Defense:<span id="charDefense"><?php echo $player->defense?></span>
        </p>
        <p>
            <span class="distributePoints">
                <button type="button" class="pointsButton" onclick="distribute('ag','+')">+</button>
                <span id="pointsDistributedAg">0</span>
                <button type="button" class="pointsButton" onclick="distribute('ag','-')">-</button>
            </span>
            Agility:<span id="charAgility"><?php echo $player->agility?></span>  
        </p>
        <p>
            <button type="button" id="distribute" onclick="givePoints('distribute','<?php echo $player->remainingPoints ?>')">Distribute</button>
            <button type="button" id="save" onclick="givePoints('save')">Save</button>
        </p>
    </div>
</div>
