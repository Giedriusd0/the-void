<?php
require_once '../../config.php';
require_once GAME_CLASSES_FOLDER . 'sql.php';

$username = $_POST['register_username'] ?? null;
$password = $_POST['register_password'] ?? null;
$email = $_POST['register_email'] ?? null;

if ($username !== null){
    if($password !== null){
        if($email !== null){
            $result = mysqli_fetch_assoc(SQL::create_account($username, password_hash($password, PASSWORD_DEFAULT), $email));
            echo $result['response'];
            unset($result);
        } else {
            echo "EMAIL NOT RECEIVED";
        }
    } else {
        echo "PASSWORD NOT RECEIVED.";
    }
} else {
    echo "USERNAME NOT RECEIVED.";
}