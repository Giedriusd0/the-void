<?php
require_once '../../config.php';
require_once GAME_CLASSES_FOLDER . 'sql.php';
require_once GAME_CLASSES_FOLDER . 'Player.php';

session_start();
// Check all values
if(isset($_POST['attack']) && isset($_POST['strength']) && isset($_POST['health']) && isset($_POST['defense']) && isset($_POST['agility']) && isset($_POST['freePoints'])){
    // Update current session 
    $player = $_SESSION['player'];
    $player->attack = $_POST['attack'];
    $player->strength = $_POST['strength'];
    $player->health = $_POST['health'];
    $player->defense = $_POST['defense'];
    $player->agility = $_POST['agility'];
    $player->remainingPoints = $_POST['freePoints'];
    $_SESSION['player'] = $player;
    // Update db
    $result = SQL::set_account_stats($player->id, $player->attack, $player->strength, $player->health, $player->defense, $player->agility, $player->remainingPoints);   
    if($result['response'] == true){
        echo true;
    } else {
        echo $result;
    }
}
?>