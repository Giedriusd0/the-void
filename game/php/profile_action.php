<?php
require_once '../../config.php';
require_once GAME_CLASSES_FOLDER . 'sql.php';
require_once GAME_CLASSES_FOLDER . 'player.php';
require_once GAME_CLASSES_ITEMS_FOLDER . 'weapon.php';
require_once GAME_CLASSES_ITEMS_FOLDER . 'shield.php';
require_once GAME_CLASSES_ITEMS_FOLDER . 'torsoArmour.php';
require_once GAME_CLASSES_ITEMS_FOLDER . 'headArmour.php';
require_once GAME_CLASSES_ITEMS_FOLDER . 'legsArmour.php';

session_start();
$player = $_SESSION['player'];

switch($_POST['action'] ?? null){
    case 'equip': {
        if(isset($_POST['slot'])){
            if($player->inventory[$_POST['slot']]->id != -1){
                $player->equip_item($_POST['slot']);
                echo true;
            }
        } else {
            echo "Slot is not set.";
        }
        break;
    }
    case 'drop': {
        if(isset($_POST['slot'])){
            $player->drop_from_inventory_slot($_POST['slot']);
            echo true;
        }
        break;
    }
    case 'unequip': {
        if(isset($_POST['equipment'])){
            $response = $player->unequip_item($_POST['equipment']);
            echo $response;
        }
        break;
    }
    default: echo "Invalid action.";
}