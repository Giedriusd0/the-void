<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    
session_start();

require_once '../../config.php';
require_once GAME_CLASSES_FOLDER . 'sql.php';
require_once GAME_CLASSES_FOLDER . 'player.php';


if(isset($_POST['account_username']) && isset($_POST['account_password'])){
    $account = SQL::get_account_password_by_name($_POST['account_username']); // Check password
    if(isset($account['response']) && $account['response'] == 'username_not_taken'){
        echo create_response('username');
    }
    else {
        if(password_verify($_POST['account_password'], $account['password'])){
            $_SESSION['player'] = new Player($account['id']);
            echo true;
        } else {
            echo create_response('password');
        }
    }
} else {
    echo "Bad inputs.";
}
function create_response(string $error){
    $response = new stdClass();
    $response->error = $error;
    switch($error){
        case 'username': $response->message = 'Username is not taken.'; break;
        case 'password': $response->message = 'Incorrect password.'; break;
    }
    $responseJson = json_encode($response);

    return $responseJson;
}
?>
     