<?php
require_once 'HeadArmour.php';
require_once 'TorsoArmour.php';
require_once 'LegsArmour.php';
require_once 'Weapon.php';
require_once 'Shield.php';
require_once 'NullItem.php';

class Item {
    public static function create_item(int $id){
        $result = SQL::get_item($id);
        switch($result['type']){
            case 'HeadArmour': return new HeadArmour($result);
            case 'TorsoArmour': return new TorsoArmour($result);
            case 'LegsArmour': return new LegsArmour($result);
            case 'Weapon': return new Weapon($result);
            case 'Shield': return new Shield($result);
            case 'NULL': return new NullItem();
        }
    }
}