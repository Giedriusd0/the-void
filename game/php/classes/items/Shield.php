<?php

class Shield {
    private $id;
    private $name;
    private $description;
    private $minDefense;
    private $maxDefense;
    private $imagePath;

    public function __construct($result){
        $this->id = $result['id'];
        $this->name = $result['name'];
        $this->description = $result['description'];
        $this->minDefense = $result['fromValue'];
        $this->maxDefense = $result['toValue'];
        // Image
        if(file_exists("../img/shield/$this->id.png")) $this->imagePath = "img/shield/$this->id.png";
        else $this->imagePath = "img/shield/default.png";
        unset($result);
    }
    public function __get(string $propertyName){
        switch($propertyName){
            case 'id': return $this->id;
            case 'name': return $this->name;
            case 'description': return $this->description;
            case 'minDefense': return $this->minDefense;
            case 'maxDefense': return $this->maxDefense;
            case 'defense': return rand($this->minDefense, $this->minDefense);
            case 'imagePath': return $this->imagePath;
        }
    }
}

?>