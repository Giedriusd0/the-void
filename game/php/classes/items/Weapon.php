<?php

class Weapon {
    private $id;
    private $name;
    private $description;
    private $minDamage;
    private $maxDamage;
    private $imagePath;

    public function __construct($result){
        $this->id = $result['id'];
        $this->name = $result['name'];
        $this->description = $result['description'];
        $this->minDamage = $result['fromValue'];
        $this->maxDamage = $result['toValue'];
        // Image
        if(file_exists("../img/weapon/$this->id.png")) $this->imagePath = "img/weapon/$this->id.png";
        else $this->imagePath = "img/weapon/default.png";
        unset($result);
    }
    public function __get(string $propertyName){
        switch($propertyName){
            case 'id': return $this->id;
            case 'name': return $this->name;
            case 'description': return $this->description;
            case 'minDamage': return $this->minDamage;
            case 'maxDamage': return $this->maxDamage;
            case 'damage': return rand($this->minDamage, $this->maxDamage);
            case 'imagePath': return $this->imagePath;
        }
    }
}

?>