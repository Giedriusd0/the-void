<?php
class SQL {
    // --------------------
    // SQL information
    // --------------------
    private static $sql_server = 'localhost';
    private static $sql_database = 'thevoid';
    private static $sql_username = 'main_user';
    private static $sql_password = 'stud007';

    // -------------------
    // Execution
    // -------------------
    private static function execute(string $query){
        $connection = new mysqli(self::$sql_server, self::$sql_username, self::$sql_password, self::$sql_database);

        // Check for connection errors
        if($connection->connect_error){
            self::to_log($connection->connect_error);
            $connection->close();
            die();
        } else {
            $result = $connection->query($query);
            if($connection->error != null){
                self::to_log($connection->error, $query);
                die();
            }
            $connection->close();
            return $result;
        }
    }

    // ------------------
    // SQL error loggin
    // ------------------
    private static function to_log(string $error, string $query = null){
        $logPath = ROOT_FOLDER . '/logs/sql_log.txt';

        if(is_array($error)){
            $error = implode(',', $error);
        }
        // Add used query if exists
        $error = $query === null ? $error : $error .= ' on query ' . $query;
        // Write to file
        if(file_exists($logPath)){
            $log = file_get_contents($logPath);
            $log .= $error . "\r\n";
        } else {
            $log = $error . "\r\n";
        }
        file_put_contents($logPath, $log);
    }
    // ------------------
    // SQL CALLS
    // ------------------
    public static function create_account(string $username, string $password, string $email){
        return self::execute("CALL create_account('$username', '$password', '$email');");
    }
    public static function get_account_full(int $id){
        return mysqli_fetch_assoc(self::execute("CALL get_account_all('$id');"));
    }
    public static function get_account_password_by_name(string $username){
        return mysqli_fetch_assoc(self::execute("CALL get_account_password_by_name('$username');"));
    }
    public static function get_item(int $id){
        return mysqli_fetch_assoc(self::execute("CALL get_item('$id');"));
    }
    public static function get_zone(int $id){
        return mysqli_fetch_assoc(self::execute("CALL get_zone('$id');"));
    }
    public static function set_account_email(int $id, string $email){
        return mysqli_fetch_assoc(self::execute("CALL update_account_email('$id', '$email');"));
    }
    public static function set_account_password(int $id, string $password){
        return mysqli_fetch_assoc(self::execute("CALL update_account_password('$id', '$password');"));
    }
    public static function set_account_stats(int $id, int $attack, int $strength, int $vitality, int $defense, int $agility, int $remainingPoints){
        return mysqli_fetch_assoc(self::execute("CALL update_account_stats('$id', '$attack', '$strength', '$vitality', '$defense', '$agility', '$remainingPoints');"));
    }
    public static function set_account_experience(int $id, int $experience){
        return self::execute("CALL update_account_experience('$id', '$experience');");
    }
    public static function set_account_remainingPoints(int $id, int $remainingPoints){
        return self::execute("CALL update_account_remainingPoints('$id', '$remainingPoints');");
    }
    public static function set_account_currentZone(int $id, int $zoneId){
        return self::execute("CALL update_account_currentZone('$id', '$zoneId');");
    }
    public static function set_account_level_up(int $id, int $level, int $experience, int $remainingPoints){
        return self::execute("CALL update_account_level_up('$id', '$level', '$experience', '$remainingPoints');");
    }
    public static function set_account_torsoArmour(int $id, int $itemId){
        return self::execute("CALL update_account_torsoArmour('$id', '$itemId');");
    }
    public static function set_account_headArmour(int $id, int $itemId){
        return self::execute("CALL update_account_headArmour('$id', '$itemId');");
    }
    public static function set_account_legsArmour(int $id, int $itemId){
        return self::execute("CALL update_account_legsArmour('$id', '$itemId');");
    }
    public static function set_account_shield(int $id, int $itemId){
        return self::execute("CALL update_account_shield('$id', '$itemId');");
    }
    public static function set_account_weapon(int $id, int $itemId){
        return self::execute("CALL update_account_weapon('$id', '$itemId');");
    }
    public static function set_account_inventory_slot(int $id, string $slot, int $itemId){
        return self::execute("CALL update_account_inventory_slot('$id', '$slot', '$itemId')");
    }
    public static function get_monster(int $id){
        return mysqli_fetch_assoc(self::execute("CALL get_monster('$id');"));
    }
    public static function get_monster_by_zone(int $id){
        return mysqli_fetch_assoc(self::execute("CALL get_monster_by_zone('$id');"));
    }
    public static function get_monster_loot(int $id){
        return mysqli_fetch_assoc(self::execute("CALL get_monster_loot('$id');"));
    }
}