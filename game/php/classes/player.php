<?php
require_once GAME_CLASSES_FOLDER . 'SQL.php';
require_once GAME_CLASSES_ITEMS_FOLDER . 'TorsoArmour.php';
require_once GAME_CLASSES_ITEMS_FOLDER . 'HeadArmour.php';
require_once GAME_CLASSES_ITEMS_FOLDER . 'LegsArmour.php';
require_once GAME_CLASSES_ITEMS_FOLDER . 'Shield.php';
require_once GAME_CLASSES_ITEMS_FOLDER . 'Weapon.php';
require_once GAME_CLASSES_ITEMS_FOLDER . 'Item.php';
require_once GAME_CLASSES_FOLDER . 'Zone.php';

class Player {
    // Details
    private $id;
    private $name;
    private $imagePath;

    // Stats
    private $level;
    private $attack;
    private $strength;
    private $vitality;
    private $hitpoints;
    private $currentHitpoints;
    private $defense;
    private $agility;
    private $experience;
    private $experienceNext;
    private $currentZone;
    private $remainingPoints;

    // Equipment
    private $headArmour;
    private $torsoArmour;
    private $legsArmour;
    private $weapon;
    private $shield;

    // Inventory
    private $gold;
    private $inventory = [];
    
    // ---------------- CONSTRUCTOR --------------------------- //
    public function __construct(int $id){
        // Details
        $result = SQL::get_account_full($id);
        $this->id = $result['id'];
        $this->name = $result['username'];
        // Stats
        $this->level = $result['statLevel'];
        $this->attack = $result['statAttack'];
        $this->strength = $result['statStrength'];
        $this->vitality = $result['statVitality'];
        $this->defense = $result['statDefense'];
        $this->agility = $result['statAgility'];
        $this->experience = $result['statExperience'];
        $this->remainingPoints = $result['statRemainingPoints'];
        $this->gold = $result['statGold'];
        $this->currentZone = new Zone((int)$result['statCurrentZone']);
        // Equipment
        $this->headArmour = Item::create_item((int)$result['equipHead']);
        $this->torsoArmour = Item::create_item((int)$result['equipTorso']);
        $this->legsArmour = Item::create_item((int)$result['equipLegs']);
        $this->weapon = Item::create_item((int)$result['equipWeapon']);
        $this->shield = Item::create_item((int)$result['equipShield']);
        // Inventory 
        $this->inventory = array(
            "slot0" => Item::create_item((int)$result["slot0"]),
            "slot1" => Item::create_item((int)$result["slot1"]),
            "slot2" => Item::create_item((int)$result["slot2"]),
            "slot3" => Item::create_item((int)$result["slot3"]),
            "slot4" => Item::create_item((int)$result["slot4"]),
            "slot5" => Item::create_item((int)$result["slot5"]),
            "slot6" => Item::create_item((int)$result["slot6"]),
            "slot7" => Item::create_item((int)$result["slot7"]),
        );
        // Experience to next
        $this->experienceNext = (((floor($this->level) + 1) * 100) - $this->experience);
        // Hitpoints
        $this->hitpoints = $this->currentHitpoints =  $this->vitality * 10;
        // Profile picture
        if(file_exists("../img/account/$id.png")){
            $this->imagePath = "img/account/$id.png";
        } else {
            $this->imagePath = "img/account/default.png";
        }
        // Unset
        unset($result);
    }

    // ---------------- GET -------------------------------- //
    public function __get($name){
        switch((string)$name){
            case 'id': return $this->id;
            case 'name': return $this->name;
            case 'imagePath': return $this->imagePath;
            case 'remainingPoints': return $this->remainingPoints;
            case 'level': return floor($this->level);
            case 'attack': return $this->attack;
            case 'strength': return $this->strength;
            case 'vitality': return $this->vitality;
            case 'hitpoints': return $this->hitpoints;
            case 'currentHitpoints': return $this->currentHitpoints;
            case 'defense': return $this->defense;
            case 'agility': return $this->agility;
            case 'experience': return $this->experience;
            case 'experienceNext': return $this->experienceNext;
            case 'headArmour': return $this->headArmour;
            case 'torsoArmour': return $this->torsoArmour;
            case 'legsArmour': return $this->legsArmour;
            case 'weapon': return $this->weapon;
            case 'shield': return $this->shield;
            case 'gold': return $this->gold;
            case 'inventory': return $this->inventory;
            case 'currentZone': return $this->currentZone;
            case 'damage': {
                // Damage by strength stat
                $damage = $this->strength;
                // Damage by weapon
                $damage += $this->weapon->damage ?? 0;
                // Damage by shield
                $damage += $this->shield->damage ?? 0;

                return $damage;
            }
            case 'totalDefense': {
                // Defense by defense stat
                $totalDefense = $this->defense;
                // Defense bonus from shield
                $totalDefense += $this->shield->defense ?? 0;
                // Defense bonus from head armour
                $totalDefense += $this->headArmour->defense ?? 0;
                // Defense bonus from torso armour
                $totalDefense += $this->torsoArmour->defense ?? 0;
                // Defense bonus from legs armour
                $totalDefense += $this->legsArmour->defense ?? 0;
        
                return $totalDefense;
            }
            default: return "Error on switch in get.(Player class)";
        }
    }
    // ---------------- SET -------------------------------- //
    public function __set($name, $value){
        switch((string)$name){
            case 'currentHitpoints': {
                $this->currentHitpoints = $value; 
                break;
            }
            case 'attack': {
                $this->attack = $value; 
                break;
            }
            case 'strength': {
                $this->strength = $value; 
                break;
            }
            case 'health': {
                $this->health = $value; 
                break;
            }
            case 'defense': {
                $this->defense = $value; 
                break;
            }
            case 'agility': {
                $this->agility = $value; 
                break;
            }
            case 'remainingPoints': {
                $this->remainingPoints = $value; 
                break;
            }
            case 'experience': {
                $this->experience = $value; 
                // Check if level need to update
                if((($this->level + 1) * 100) < $this->experience){//level = (sqrt(100(2experience+25))+50)/100
                    // Update level
                    //$this->level = floor((sqrt(100*(2*$this->experience + 25))+50)/100); //$this->experience / 100);
                    $this->level = floor($this->experience / 100);
                    // Add remainingPoints
                    $this->remainingPoints += 5;
                    // Update database
                    SQL::set_account_level_up($this->id, $this->level, $this->experience, $this->remainingPoints);
                    // Check if zone need updating
                    if(($this->level) % 5 == 0)
                        $this->update_zone();
                } else {
                    // Update experience
                    SQL::set_account_experience($this->id, $this->experience);
                }
                // Update experience to next level
                $this->experienceNext = (((floor($this->level) + 1) * 100) - $this->experience);
                break;
            }
        }
    }
    // ---------------- METHODS ---------------------------- //
    public function check_image(){
        if(file_exists("../img/account/$this->id.png")){
            $this->imagePath = "img/account/$this->id.png";
        } else {
            $this->imagePath = "img/account/default.png";
        }
        return $this->imagePath;
    }
    private function update_zone()
    {
        if($this->level >= 5 && $this->currentZone->id == 1)
        {
            $this->currentZone = new Zone(2);
            SQL::set_account_currentZone($this->id, $this->currentZone->id);
        }
        if($this->level >= 10 && $this->currentZone->id == 2)
        {
            $this->currentZone = new Zone(3);
            SQL::set_account_zone($this->id, $this->currentZone->id);
        }
        if($this->level >= 15 && $this->currentZone->id == 3)
        {
            $this->currentZone = new Zone(4);
            SQL::set_account_zone($this->id, $this->currentZone->id);
        }
        if($this->level >= 20 && $this->currentZone->id == 4)
        {
            $this->currentZone = new Zone(5);
            SQL::set_account_zone($this->id, $this->currentZone->id);
        }
        if($this->level >= 25 && $this->currentZone->id == 5)
        {
            $this->currentZone = new Zone(6);
            SQL::set_account_zone($this->id, $this->currentZone->id);
        }
    }
    public function equip_item(string $slot){
        // Item to equip
        $temp = $this->inventory[$slot];
        // Switch depending on object, to know where to equip
        switch(get_class($temp)){
            case 'TorsoArmour': {
                // Put current item into inventory 
                $this->inventory[$slot] = $this->torsoArmour;
                // Equip new item from inventory
                $this->torsoArmour = $temp;
                // Update database on torsoArmour
                SQL::set_account_torsoArmour($this->id, $this->torsoArmour->id);
                break;
            }
            case 'HeadArmour': {
                $this->inventory[$slot] = $this->headArmour;
                $this->headArmour = $temp; 
                SQL::set_account_headArmour($this->id, $this->headArmour->id);
                break;
            }
            case 'LegsArmour': {
                $this->inventory[$slot] = $this->legsArmour;
                $this->legsArmour = $temp;
                SQL::set_account_legsArmour($this->id, $this->legsArmour->id);
                break;
            }
            case 'Shield': {
                $this->inventory[$slot] = $this->shield;
                $this->shield = $temp;
                SQL::set_account_shield($this->id, $this->shield->id);
                break;
            }
            case 'Weapon': {
                $this->inventory[$slot] = $this->weapon;
                $this->weapon = $temp;
                SQL::set_account_weapon($this->id, $this->weapon->id);
                break;
            }
        }
        // Update inventory slot
        SQL::set_account_inventory_slot($this->id, $slot, $this->inventory[$slot]->id);
    }
    public function unequip_item($slot){
        switch($slot){
            case 'HeadArmour': {
                // Move item to inventory
                if($this->add_to_inventory($this->headArmour->id)){
                    // Remove item
                    $this->headArmour = new NullItem();
                    // Update database
                    SQL::set_account_headArmour($this->id, $this->headArmour->id);
                    return true;
                } else {
                    // No space in inventory
                    return 'no_space';
                }
                break;
            }
            case 'TorsoArmour': {
                if($this->add_to_inventory($this->torsoArmour->id)){
                    $this->torsoArmour = new NullItem();
                    SQL::set_account_torsoArmour($this->id, $this->torsoArmour->id);
                    return true;
                } else {
                    return 'no_space';
                }
                break;
            }
            case 'LegsArmour': {
                if($this->add_to_inventory($this->legsArmour->id)){
                    $this->legsArmour = new NullItem();
                    SQL::set_account_legsArmour($this->id, $this->legsArmour->id);
                    return true;
                } else {
                    return 'no_space';
                }
                break;
            }
            case 'Shield': {
                if($this->add_to_inventory($this->shield->id)){
                    $this->shield = new NullItem();
                    SQL::set_account_shield($this->id, $this->shield->id);
                    return true;
                } else {
                    return 'no_space';
                }
                break;
            }
            case 'Weapon': {
                if($this->add_to_inventory($this->weapon->id)){
                    $this->weapon = new NullItem();
                    SQL::set_account_weapon($this->id, $this->weapon->id);
                    return true;
                } else {
                    return 'no_space';
                }
                break;
            }
        }
    }
    public function drop_from_inventory_slot($slot){
        // Switch to null item
        $this->inventory[$slot] = new NullItem();
        // Update database
        SQL::set_account_inventory_slot($this->id, $slot, 1);
    }
    public function add_to_inventory($id){
        // Check inventory for free space
        foreach($this->inventory as $key => $item){
            if(get_class($item) == 'NullItem'){
                // Add item
                $this->inventory[$key] = Item::create_item($id);
                // Update database
                SQL::set_account_inventory_slot($this->id, $key, $this->inventory[$key]->id);
                return true;
            }
        }
        return false;
    }
}