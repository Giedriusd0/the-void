<?php
require_once GAME_CLASSES_FOLDER . '/sql.php';
require_once GAME_CLASSES_FOLDER . '/Zone.php';

class Monster {
    // Details
    private $id;
    private $name;
    private $level;
    private $zone;
    private $description;
    private $imagePath;
    // Stats
    private $attack;
    private $hitpoints;
    private $currentHitpoints;
    private $defense;
    // Rewards
    private $experience;
    private $gold;
    // Loot
    private $loot;

    // ---------------- CONSTRUCTOR --------------------------- //
    function __construct(int $id){
        $result = SQL::get_monster($id);
        $this->id = $id;
        $this->name = $result['monstName'];
        $this->level = $result['monstLevel'];
        $this->zone = $result['monstZone'];
        $this->attack = $result['monstAttack'];
        $this->hitpoints = $result['monstHitpoints'];
        $this->currentHitpoints = $result['monstHitpoints'];
        $this->defense = $result['monstDefense'];
        $this->gold = $result['monstGold'];
        $this->description = $result['monstDescription'];
        $this->experience = $result['monstExperience'];

        // Image
        if(file_exists("img/monster/$id.png")){
            $this->imagePath = "img/monster/$id.png";
        } else {
            $this->imagePath = "img/monster/default.png";
        }
        unset($result);
        // Loot
        $loot = SQL::get_monster_loot($id);

    }
    // ----------------- GET ----------------------------------- //
    public function __get($name){
        switch($name){
            case 'id': return $this->id;
            case 'name': return $this->name;
            case 'level': return $this->level;
            case 'zone': return $this->zone;
            case 'description': return $this->description;
            case 'imagePath': return $this->imagePath;
            case 'attack': return $this->attack;
            case 'hitpoints': return $this->hitpoints;
            case 'currentHitpoints': return $this->currentHitpoints;
            case 'defense': return $this->defense;
            case 'experience': return $this->experience;
            case 'gold': return $this->gold;
            case 'loot': return $this->loot;
            case 'damage': {
                return $this->attack;
            }
            case 'totalDefense': {
                return $this->defense;
            }
            default: return "Error on switch in get (Monster Class)";
        }
    }
    // ----------------- SET ---------------------------------- //
    public function __set($name, $value){
        if((string)$name == 'currentHitpoints'){
            $this->currentHitpoints = $value;
        }
    }
}