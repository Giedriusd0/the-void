<?php
require_once GAME_CLASSES_FOLDER . '/sql.php';

class Zone {
    private $id;
    private $name;
    private $description;
    private $imagePath;

    public function __construct(int $id){
        $result = SQL::get_zone($id);
        $this->id = $id;
        $this->name = $result['name'];
        $this->description = $result['description'];
        $this->imagePath = "img/zone/$id.png";
    }
    public function __get($propertyName){
        switch($propertyName){
            case 'id': return $this->id;
            case 'name': return $this->name;
            case 'description': return $this->description;
            case 'imagePath': return $this->imagePath;
            default: return 'Error: Zone class on Switch.';
        }
    }
}