<?php
require_once '../../config.php';
require_once GAME_CLASSES_FOLDER . 'sql.php';
require_once GAME_CLASSES_FOLDER . 'player.php';

session_start();

$id = $_SESSION['player']->id;
// Check files
if(isset($_FILES['newAvatar']['name'])) $_POST['action'] = 'changeAvatar';

switch($_POST['action'] ?? null){
    case 'changeEmail': {
        if(isset($_POST['new_email'])){
            $result = SQL::set_account_email($id, $_POST['new_email']);
            // Response
            if($result['response'] == 0) echo "Email has been changed.";
            else if ($result['response'] == 1) echo "Email already in use.";
            else echo "Something happened. Something not good.";
        } else {
            echo "ERROR EMAIL NOT FOUND.";
        }
        break;
    }
    case 'changePassword': {
        if(isset($_POST['new_password'])){
            $result = SQL::set_account_password($id, password_hash($_POST['new_password'], PASSWORD_DEFAULT));
            // Response
            if($result['response'] == 0) echo "Password has been changed.";
            else echo "Something happened. Something not good.";
        } else {
            echo "ERROR PASSWORD NOT FOUND.";
        }
        break;
    }
    case 'changeAvatar': {
        // Location to upload
        $targetLocation = GAME_IMAGES_FOLDER . "account/";
        // Direct file location
        $targetFile = $targetLocation . basename($_FILES["newAvatar"]["name"]);
        // Get file type
        $imageFileType = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["newAvatar"]["tmp_name"]);
            if($check !== false) {
                if($check[0] <= 500 && $check[1] <= 500){
                    if($check[0] / $check[1] == 1){
                        if (move_uploaded_file($_FILES["newAvatar"]["tmp_name"], $targetLocation . $_SESSION['player']->id . ".png")) {
                            echo create_response(true, $_SESSION['player']->check_image());
                        } else {
                            echo create_response(false, "Upload error.");
                        }
                    } else {
                        echo create_response(false, "Image dimensions ratio is not 1.");
                    }
                } else {
                    echo create_response(false, "Image dimensions are too big.");
                }
            } else {
                echo create_response(false, "File format unsupported.");
            }
        break;
    }
    default: echo 'ERROR ON CHANGE_SETTINGS SWITCH: UNKOWN ACTION.';
}
function create_response(bool $response, string $message){
        // Create response
        $json = new stdClass();
        $json->response = $response;
        $json->message = $message;
        // Encode
        $responseJson = json_encode($json);
        return $responseJson;
}
?>