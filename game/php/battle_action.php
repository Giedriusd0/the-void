<?php
require_once '../../config.php';
require_once GAME_CLASSES_FOLDER . 'player.php';
require_once GAME_CLASSES_FOLDER . 'monster.php';

session_start();
// Check if action exists
// --------------- SEQUENCE -------------- \\
if(isset($_POST['action'])){
    $player = $_SESSION['player'];
    $enemy = $_SESSION['enemy'];
    $log = '';
    $playerDamage = 0;
    $monsterDamage = 0;
    $monsterAttack = true;
    $playerAttack = true;

    switch($_POST['action']){
        case 'attack': {
            $playerAttack = true;
            // Player attacks, check if win
            if(player_attack()){
                // Win situation
                player_receive_reward();
                echo create_response('win');
                unset($enemy);
            } else {
                // Monster attacks, check if loss
                if(monster_attack()){
                    // Lose situation
                    echo create_response('lose');
                    unset($enemy);
                } else {
                    echo create_response();
                }
            }
            break;
        }
        case 'defend': {
            $playerAttack = false;
            $log .= "You choose to defend.<br>";
            if(monster_attack()){
                // Lose situation
                echo create_response('lose');
                unset($enemy);
            } else {
                echo create_response();            
            }
            break;
        }
        case 'run': {
            // Escape
            $log .= "You choose to run away.";
            echo create_response();
            unset($enemy);
            break;
        }
        default : echo "ERROR: ACTION IN SWITCH NOT FOUND.";
    }
    // Update player in session
    $_SESSION['player'] = $player;
    // Update enemy in session
    if(isset($enemy)) $_SESSION['enemy'] = $enemy; 
        else unset($_SESSION['enemy']);

} else {
    echo "ERROR: ACTION NOT FOUND.";
}
// ----------------- FUNCTIONS ----------- \\
function create_response(string $stage = null, string $attacker = null){
    global $player, $enemy, $log, $playerDamage, $monsterDamage;
    // Check stage
    if($stage == 'win') $log .= "You win.<br>";
    else if ($stage == 'lose') $log .= "You lose.<br>";

    // Create response
    $response = new stdClass();
    $response->playerCurrentHealth = $player->currentHitpoints;
    $response->enemyCurrentHealth = $enemy->currentHitpoints;
    $response->playerHealth = $player->hitpoints;
    $response->enemyHealth = $enemy->hitpoints;
    $response->playerDamage = $playerDamage;
    $response->monsterDamage = $monsterDamage;
    $response->battle_log = $log;
    // Encode
    $responseJson = json_encode($response);
    return $responseJson;
}
function player_attack(){
    global $player, $enemy, $log, $playerDamage, $monsterDamage, $monsterAttack;

    // Chance to hit
    $monsterChanceToEvade = rand(0, 100);
    $chanceToHit = 100-($enemy->level - ($player->level)) + $player->attack;
    //Is monster defending
    if(rand(0, 4) > 3)  $monsterAttack = false;
     else $monsterAttack = true;
    // Damage
    if(($chanceToHit > 0)&&($monsterChanceToEvade < $chanceToHit)){
        // Final damage
        if ($monsterAttack) 
        {
            $damage = $player->damage;
            // Check if not minus
            if($damage < 0) $damage = 0;
            // Apply damage
            $enemy->currentHitpoints -= $damage;
            $playerDamage = $damage;
            // Log it
            $log .= "You dealt $damage damage to $enemy->name.<br>";
        }
         else 
         {
             $damage = $player->damage - $enemy->defense;
             // Check if not minus
            if($damage < 0) $damage = 0;
            // Apply damage
            $enemy->currentHitpoints -= $damage;
            $playerDamage = $damage;
            // Log it
            $log .= "The $enemy->name attempted to defend itself! You dealt $damage damage to $enemy->name.<br>";
         }
        
    } else {
        // You missed. Ouch.
        $log .= "You slipped and missed $enemy->name.<br>";
    }
    $currentDamage = $damage;
    // Check if hp not bellow 0
    if($enemy->currentHitpoints < 0) $enemy->currentHitpoints = 0;
    // Check if win
    return $enemy->currentHitpoints == 0 ? true : false;
}
function monster_attack(){
    global $enemy, $player, $log, $monsterAttack, $playerDamage, $monsterDamage, $playerAttack, $currentDamage;

    if($monsterAttack)
    {
        $extraDefense = 0;
        if ($playerAttack != true) $extraDefense += $player->totalDefense;
        // Final damage
        $damage = ($enemy->damage - $player->totalDefense - $extraDefense);
        // Check if not bellow 0
        if($damage < 0) $damage = 0;
        // Apply damage
        $player->currentHitpoints -= $damage;
        $monsterDamage = $damage;
        // Log
        $log .= "$enemy->name dealt $damage damage to you.<br>";
        // Check if hp not bellow 0
        if($player->currentHitpoints < 0) $player->currentHitpoints = 0;
    }
    else {
        $log .= "$enemy->name was too busy defending to attack you!<br>";
        $currentDamage = 0;
    } 
    // Check if loss
    return $player->currentHitpoints == 0 ? true : false;
}
function player_receive_reward(){
    global $player, $enemy, $log;

    // Gib experience
    $player->experience += $enemy->experience;
    // Gib cash
    $player->gold += $enemy->gold;
    // Check if leveled up
    if($player->level == ($_SESSION['player']->level + 1))
        $log .= "You leveled up!<br>";
}