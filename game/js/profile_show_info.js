function showInfo(item){
    const overlay = document.getElementById("overlayInfo");
    const headArmour = document.getElementById("equipHeadInfo");
    const torsoArmour = document.getElementById("equipTorsoInfo");
    const legsArmour = document.getElementById("equipLegsInfo");
    const weapon = document.getElementById("equipWeaponInfo");
    const shield = document.getElementById("equipShieldInfo");
    const slot0 = document.getElementById("slot0Info");
    const slot1 = document.getElementById("slot1Info");
    const slot2 = document.getElementById("slot2Info");
    const slot3 = document.getElementById("slot3Info");
    const slot4 = document.getElementById("slot4Info");
    const slot5 = document.getElementById("slot5Info");
    const slot6 = document.getElementById("slot6Info");
    const slot7 = document.getElementById("slot7Info");


    switch(item){
        case 'HeadArmour': {
            overlay.style.display = "block";
            headArmour.style.display = "block";
            break;
        }
        case 'TorsoArmour': {
            overlay.style.display = "block";
            torsoArmour.style.display = "block";
            break;
        }
        case 'LegsArmour': {
            overlay.style.display = "block";
            legsArmour.style.display = "block";
            break;
        }
        case 'Weapon': {
            overlay.style.display = "block";
            weapon.style.display = "block";
            break;
        }
        case 'Shield': {
            overlay.style.display = "block";
            shield.style.display = "block";
            break;
        }
        case 'slot0': {
            overlay.style.display = "block";
            slot0.style.display = "block";
            break;
        }
        case 'slot1': {
            overlay.style.display = "block";
            slot1.style.display = "block";
            break;
        }
        case 'slot2': {
            overlay.style.display = "block";
            slot2.style.display = "block";
            break;
        }
        case 'slot3': {
            overlay.style.display = "block";
            slot3.style.display = "block";
            break;
        }
        case 'slot4': {
            overlay.style.display = "block";
            slot4.style.display = "block";
            break;
        }
        case 'slot5': {
            overlay.style.display = "block";
            slot5.style.display = "block";
            break;
        }
        case 'slot6': {
            overlay.style.display = "block";
            slot6.style.display = "block";
            break;
        }
        case 'slot7': {
            overlay.style.display = "block";
            slot7.style.display = "block";
            break;
        }
        case 'hide': {
            overlay.style.display = "none";
            headArmour.style.display = "none";
            torsoArmour.style.display = "none";
            legsArmour.style.display = "none";
            weapon.style.display = "none";
            shield.style.display = "none";
            slot0.style.display = "none";
            slot1.style.display = "none";
            slot2.style.display = "none";
            slot3.style.display = "none";
            slot4.style.display = "none";
            slot5.style.display = "none";
            slot6.style.display = "none";
            slot7.style.display = "none";
            break;
        }
    }
}