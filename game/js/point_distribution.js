var spentPoints = 0;
var allPoints = 0;
var totalPoints = null;

function givePoints(option, points = null)
{
    const pointsForms = Array.from(document.getElementsByClassName("distributePoints"));
    const distributeButton = document.getElementById("distribute");
    const saveButton = document.getElementById("save");
    if(points !== null) allPoints = points;
    if(totalPoints == null) totalPoints = points;
    switch(option)
    {
        case 'distribute':
            pointsForms.forEach(element => {
                element.style.display = 'inline-block';
            });
            distributeButton.style.display = 'none';
            saveButton.style.display = 'block';
        break;

        case 'save': {
            // New points
            const AttackPoints = parseInt(document.getElementById("pointsDistributedAtt").innerHTML) + parseInt(document.getElementById("charAttack").innerHTML);
            const StrengthPoints = parseInt(document.getElementById("pointsDistributedStr").innerHTML) + parseInt(document.getElementById("charStrength").innerHTML);
            const HealthPoints = parseInt(document.getElementById("pointsDistributedH").innerHTML) + parseInt(document.getElementById("charHealth").innerHTML);
            const DefensePoints = parseInt(document.getElementById("pointsDistributedDef").innerHTML) + parseInt(document.getElementById("charDefense").innerHTML);
            const AgilityPoints = parseInt(document.getElementById("pointsDistributedAg").innerHTML) + parseInt(document.getElementById("charAgility").innerHTML);
            const RemainingPoints = (parseInt(totalPoints) - parseInt(spentPoints));
            // Vars for request
            const vars = "attack=" + AttackPoints + "&strength=" + StrengthPoints + "&health=" + HealthPoints + "&defense=" + DefensePoints + "&agility=" + AgilityPoints + "&freePoints=" + RemainingPoints;
            // Send request
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if(this.readyState == 4 && this.status == 200){
                    if(this.response == true){
                        pointsForms.forEach(element => {
                            element.style.display = 'none';
                        });
                        saveButton.style.display = 'none';
                        distributeButton.style.display = 'block';
                    } else {
                        window.alert(this.response);
                    }
                }
            }
            xhttp.open('POST', 'php/update_stats.php', true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send(vars);
            // Update current html stats
            document.getElementById("charFreePoints").innerHTML = allPoints;
            document.getElementById("charAttack").innerHTML = AttackPoints;
            document.getElementById("charStrength").innerHTML = StrengthPoints;
            document.getElementById("charHealth").innerHTML = HealthPoints;
            document.getElementById("charDefense").innerHTML = DefensePoints;
            document.getElementById("charAgility").innerHTML = AgilityPoints;
            break;
        }
        default:
        break;
    }
}

function distribute(attribute, action)
{
    const canIncrease = allPoints > 0 ? true : false;
    const canDecrease = spentPoints > 0 ? true : false;
    /*const canDecrease = true;
    const canIncrease = true;
    */switch(action)
    {
        case '+':
            if(canIncrease)
                increaseAttribute(attribute);
        break;
        case '-':
            if(canDecrease)
                decreaseAttribute(attribute);
        break;
        default:
            //error
        break;
    }
}

function increaseAttribute(attribute)
{
    switch(attribute)
    {
        case 'att':
            document.getElementById("pointsDistributedAtt").innerHTML = (parseInt(document.getElementById("pointsDistributedAtt").innerHTML)+1);
        break;
        case 'str':
            document.getElementById("pointsDistributedStr").innerHTML = (parseInt(document.getElementById("pointsDistributedStr").innerHTML)+1);
        break;
        case 'h':
            document.getElementById("pointsDistributedH").innerHTML = (parseInt(document.getElementById("pointsDistributedH").innerHTML)+1);
        break;
        case 'def':
            document.getElementById("pointsDistributedDef").innerHTML = (parseInt(document.getElementById("pointsDistributedDef").innerHTML)+1);
        break;
        case 'ag':
            document.getElementById("pointsDistributedAg").innerHTML = (parseInt(document.getElementById("pointsDistributedAg").innerHTML)+1);
        break;
    }
    allPoints--;
    spentPoints++;
    document.getElementById("charFreePoints").innerHTML = allPoints;
}

function decreaseAttribute(attribute)
{
    switch(attribute)
    {
        case 'att':
            document.getElementById("pointsDistributedAtt").innerHTML = parseInt(document.getElementById("pointsDistributedAtt").innerHTML) != 0 ? (parseInt(document.getElementById("pointsDistributedAtt").innerHTML)-1) : 0;
            if(document.getElementById("pointsDistributedAtt").innerHTML != 0)
            {
                allPoints++;
                spentPoints--;
            }
        break;
        case 'str':
            document.getElementById("pointsDistributedStr").innerHTML = parseInt(document.getElementById("pointsDistributedStr").innerHTML) != 0 ? (parseInt(document.getElementById("pointsDistributedStr").innerHTML)-1) : 0;
            if(document.getElementById("pointsDistributedAtt").innerHTML != 0)
            {
                allPoints++;
                spentPoints--;
            }
        break;
        case 'h':
            document.getElementById("pointsDistributedH").innerHTML = parseInt(document.getElementById("pointsDistributedH").innerHTML) != 0 ? (parseInt(document.getElementById("pointsDistributedH").innerHTML)-1) : 0;
            if(document.getElementById("pointsDistributedAtt").innerHTML != 0)
            {
                allPoints++;
                spentPoints--;
            }
        break;
        case 'def':
            document.getElementById("pointsDistributedDef").innerHTML = parseInt(document.getElementById("pointsDistributedDef").innerHTML) != 0 ? (parseInt(document.getElementById("pointsDistributedDef").innerHTML)-1) : 0;
            if(document.getElementById("pointsDistributedAtt").innerHTML != 0)
            {
                allPoints++;
                spentPoints--;
            }
        break;
        case 'ag':
            document.getElementById("pointsDistributedAg").innerHTML = parseInt(document.getElementById("pointsDistributedAg").innerHTML) != 0 ? (parseInt(document.getElementById("pointsDistributedAg").innerHTML)-1) : 0;
            if(document.getElementById("pointsDistributedAtt").innerHTML != 0)
            {
                allPoints++;
                spentPoints--;
            }
        break;
    }
    document.getElementById("charFreePoints").innerHTML = allPoints;
}