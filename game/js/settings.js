function callSettings(option)
{
    // Overlay
    const editForm = document.getElementById("overlay");
    // Inputs
    const passEditElement = document.getElementById("passEdit");
    const passEditConfirmElement = document.getElementById("passEditConfirm");
    const emailEditElement = document.getElementById("emailEdit");
    const emailEditConfirmElement = document.getElementById("emailEditConfirm");
    // Pass values
    const passEdit = passEditElement.value.trim();
    const passEditConfirm = passEditConfirmElement.value.trim(); 
    // Email inputs
    const emailEdit = emailEditElement.value.trim();
    const emailEditConfirm = emailEditConfirmElement.value.trim();    
    // Responses
    const passResponse = document.getElementById("passResponse");
    const emailResponse = document.getElementById("emailResponse");
    // Email pattern
    const emailPattern = new RegExp(/^[^\s@]+@[^\s@]+\.[^\s@]+$/);

    switch(option){
        case 'show': editForm.style.display = 'block'; break;
        case 'hide': {
            editForm.style.display = 'none';
            // Reset values 
            passEditElement.value = '';
            passEditConfirmElement.value = '';
            emailEditElement.value = '';
            emailEditConfirmElement.value = ''; 
            break;
        }
        case 'saveEmail': {
            // Check if email format is correct 
            if(emailPattern.test(emailEdit)){
                // Check if emails are the same
                if(emailEdit === emailEditConfirm){
                    // All is good, send the request
                    var xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function() {
                        if(this.readyState == 4 && this.status == 200){
                            // Reset values
                            emailEditElement.value = '';
                            emailEditConfirmElement.value = '';
                            // Response
                            emailResponse.innerHTML = this.response;
                            setTimeout(function(){
                                emailResponse.innerHTML = '';
                            }, 3000);
                        }
                    }
                    xhttp.open('POST', 'php/change_settings.php', true);
                    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    xhttp.send("action=changeEmail&new_email=" + emailEdit);
                } else {
                    emailResponse.innerHTML = 'Emails do not match.';
                    setTimeout(function(){
                        emailResponse.innerHTML = '';
                    }, 3000);
                }
            } else {
                emailResponse.innerHTML = 'Please enter a valid email.';
                setTimeout(function(){
                    emailResponse.innerHTML = '';
                }, 3000);
            }
            break;
        } 
        case 'savePass': {
            // Check password format
            if(passEdit != '' && passEdit.length >= 6){
                // Check if passwords match
                if(passEdit === passEditConfirm){
                    // All is good, send the request
                    var xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function(){
                        if(this.readyState == 4 && this.status == 200){
                            // Reset values
                            passEditElement.value = '';
                            passEditConfirmElement.value = '';
                            // Response
                            passResponse.innerHTML = this.response;
                            setTimeout(function(){
                                passResponse.innerHTML = '';
                            }, 3000);
                        }
                    }
                    xhttp.open('POST', 'php/change_settings.php', true);
                    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    xhttp.send("action=changePassword&new_password=" + passEdit);
                } else {
                    passResponse.innerHTML = 'Passwords do not match.';
                    setTimeout(function(){
                        passResponse.innerHTML = '';
                    }, 3000);    
                }
            } else {
                passResponse.innerHTML = 'Please type in a pasword of at least 6 characters.';
                setTimeout(function(){
                    passResponse.innerHTML = '';
                }, 3000);
            }
            break;
        }
        case 'saveImage': {
            const fileToUpload = document.getElementById("newAvatar").files;
            var image = new FormData();
            image.append('newAvatar', fileToUpload[0]);

            var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function(){
                    if(this.readyState == 4 && this.status == 200){
                        const result = JSON.parse(this.response);
                        if(result.response == true){
                            document.getElementById("settingsAvatarImage").src = result.message + "?" + new Date().valueOf();
                            document.getElementById("profileAvatar").src = result.message + "?" + new Date().valueOf();
                            document.getElementById("newAvatar").value = "";
                        } else if (result.response == false) {
                            document.getElementById("avatarResponse").innerHTML = result.message;
                            setTimeout(function(){
                                document.getElementById("avatarResponse").innerHTML = "";
                            }, 3000);
                        }
                    }
                }
            xhttp.open('POST', 'php/change_settings.php', true);
            xhttp.send(image);
            break;
        }
    }
}