function unequip(equipmentType, itemId){
    if(itemId !== 1){
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if(this.readyState == 4 && this.status == 200){
                if(this.response == true) window.location.reload();
            } else if (this.response == 'no_space') {
                window.alert('No space in inventory.');
            }
        };
        xhttp.open('POST', 'php/profile_action.php', true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("action=unequip&equipment=" + equipmentType);  
    }
}