function showMenu(id, itemId) {
  if(itemId !== 1){
    elementId="myDropdown"+id;
    // Disable all
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
    
    document.getElementById(elementId).classList.toggle("show");
  }
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.inventorySlotI')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
function equip(slot){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){
      if(this.response == true) {
        window.location.reload();
      } else {
        window.alert(this.response);
      }
    }
  };
  xhttp.open('POST', 'php/profile_action.php', true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("action=equip&slot=" + slot);  
}
function drop(slot){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){
      if(this.response == true) {
        window.location.reload();
      } else {
        window.alert(this.response);
      }
    }
  };
  xhttp.open('POST', 'php/profile_action.php', true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("action=drop&slot=" + slot);
}