window.onload = function(){
    // Monster description
    var e = document.getElementById('monsterImageContainer');
    e.onmouseover = function() {
        document.getElementById('monsterDescription').style.display = 'block';
    }
    e.onmouseout = function() {
    document.getElementById('monsterDescription').style.display = 'none';
    }
}
function setHpBars(enemyHp, enemyTotalHp, playerHp, playerTotalHp){
    const PlayerCurrentHP = document.getElementById("playerCurrentHealth");
    const EnemyCurrentHP = document.getElementById("enemyCurrentHealth");
    const PlayerHealthBar = document.getElementById("playerHealthBar");
    const EnemyHealthBar = document.getElementById("enemyHealthBar");

    // Show player current HP numbers
    PlayerCurrentHP.innerHTML = playerHp + " / " + playerTotalHp;
    // Player Health bar
    PlayerHealthBar.style.width = getPercentage(playerHp, playerTotalHp) + '%';
    // Show enemy current HP numbers
    EnemyCurrentHP.innerHTML = enemyHp + " / " + enemyTotalHp;
    // Enemy health bar
    EnemyHealthBar.style.width = getPercentage(enemyHp, enemyTotalHp) + '%';
}

function action(type){
    const PlayerCurrentHP = document.getElementById("playerCurrentHealth");
    const EnemyCurrentHP = document.getElementById("enemyCurrentHealth");
    const PlayerHealthBar = document.getElementById("playerHealthBar");
    const EnemyHealthBar = document.getElementById("enemyHealthBar");
    const BattleLog = document.getElementById("battle_log");
    const Actions = document.getElementById("battleActionButtons");
    const Results = document.getElementById("battleResultButtons");
    // Splash images
    const PlayerSplashImage = document.getElementById("playerSplashDammageImage");
    const EnemySplashImage = document.getElementById("monsterSplashImage");
    // Damage ammounts
    const PlayerSplashDamage = document.getElementById("playerSplashDamageAmmount");
    const EnemySplashDamage = document.getElementById("monsterSplashAmmount");

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200){
            const result = JSON.parse(this.response);
            // Show player current HP numbers
            PlayerCurrentHP.innerHTML = result.playerCurrentHealth + " / " + result.playerHealth;
            // Player Health bar
            PlayerHealthBar.style.width = getPercentage(result.playerCurrentHealth, result.playerHealth) + '%';
            // Show enemy current HP numbers
            EnemyCurrentHP.innerHTML = result.enemyCurrentHealth + " / " + result.enemyHealth;
            // Enemy health bar
            EnemyHealthBar.style.width = getPercentage(result.enemyCurrentHealth, result.enemyHealth) + '%';
            // Splash damage
            if(result.playerDamage !== 0){
                EnemySplashImage.style.display = "block";
                EnemySplashDamage.innerHTML = result.playerDamage;
                setTimeout(function(){
                    EnemySplashImage.style.display = "none";
                    EnemySplashDamage.innerHTML = "";
                }, 1000);
            
            }
            if(result.monsterDamage !== 0){
                PlayerSplashImage.style.display = "block";
                PlayerSplashDamage.innerHTML = result.monsterDamage;
                setTimeout(function(){
                    PlayerSplashImage.style.display = "none";
                    PlayerSplashDamage.innerHTML = "";
                }, 1000);
            }
            BattleLog.innerHTML = result.battle_log;
            if(result.playerCurrentHealth == 0 || result.enemyCurrentHealth == 0 || result.battle_log.indexOf("run away") != -1){
                Actions.style.display = 'none';
                Results.style.display = 'block';
            }
        }
    };
    xhttp.open('POST', 'php/battle_action.php', true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=" + type);
}
function navigateTo(view){
    switch(view){
        case 'next': window.location.reload(); break;
        case 'profile': window.location.href = 'profile.php'; break;
    }
}
function getPercentage(num1, num2){
    return parseInt(num1 / num2 * 100);
}