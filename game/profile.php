<?php 
require_once '../config.php';
require_once GAME_CLASSES_FOLDER . 'Player.php';
session_start();
// Check if account exists
if(!isset($_SESSION['player'])){
    header('Location: ../index.php');
} 
else 
{
    $player = $_SESSION['player'];
    // Reset healthpoints when back to profile
    $player->currentHitpoints = $player->hitpoints;
    $_SESSION['player'] = $player;
}
?>
<!DOCTYPE html>
<html lang="lt">
<head>
    <meta charset="UTF-8">
    <title>The Void</title>
    <link href="css/profile_layout.css" rel="stylesheet" type="text/css">
    <link href="css/profile_skin.css" rel="stylesheet" type="text/css">

    <script src="js/settings.js"></script>
    <script src="js/point_distribution.js"></script>
    <script src="js/profile_navigation.js"></script>
    <script src="js/inventory.js"></script>
    <script src="js/equipment.js"></script>
    <script src="js/profile_show_info.js"></script>
</head>
<body style="background-image:url(<?php echo $player->currentZone->imagePath;?>); background-position:bottom">
    <div id="content">
        <div id="profileTable">
            <!-- Equipment -->            
            <?php require_once 'incl/profile_equipment.php'; ?>
            <!-- Statistics -->
            <?php require_once 'incl/profile_stats.php'; ?>
            <!-- Inventory -->
            <?php require_once 'incl/profile_inventory.php'; ?>
        </div>
        <!-- Navigaton -->
        <div id="navigation">
            <button type="button" onclick="goto('logout')">Log out</button>
            <button type="button" onclick="callSettings('show');">Settings</button>
            <button type="button" onclick="goto('nextBattle')">Next Battle</button>
        </div>
    </div>
    <div id="overlay" onclick="callSettings('hide')">
        <div id="modify" onclick="event.stopPropagation();">
            <h1 style="font-weight: bold;">Profile settings</h1>
            <h3 style="font-weight: bold;">Change avatar</h3>
            <table>
                <tr>
                    <td><img id="settingsAvatarImage" src="<?php echo $player->imagePath ?>"></td>
                    <td><p style="margin-left: .5em">Max image size: 500 x 500</p>
                        <p style="margin-left: .5em">Image should have 1:1 ratio</p>
                        <p style="margin-left: .5em">Supported formats: .png and .jpg</p></td>
                </tr>
            </table>
            <p style="font-weight: bold;">Select new image</p>
            <input type="file" id="newAvatar">
            <button id="saveSettings" type="button" onclick="callSettings('saveImage')">Upload</button>
            <p id="avatarResponse" style="font-weight: bold;"></p>
            <hr>
            <h3 style="font-weight: bold;">Change password</h3>
            <p style="font-weight: bold;">Enter new password:</p>
            <input type="password" id="passEdit" value="">
            <p style="font-weight: bold;">Confirm new password:</p>
            <input type="password" id="passEditConfirm" value="">
            <p id="passResponse" style="font-weight: bold;"></p>
            <button id="saveSettings" type="button" onclick="callSettings('savePass')">Save</button>
            <hr>
            <h3 style="font-weight: bold;">Change email</h3>
            <p style="font-weight: bold;">Enter new Email:</p>
            <input type="text" id="emailEdit" value="">
            <p style="font-weight: bold;">Confirm new Email:</p>
            <input type="text" id="emailEditConfirm" value="">
            <p id="emailResponse" style="font-weight: bold;"></p>
            <button id="saveSettings" type="button" onclick="callSettings('saveEmail')">Save</button>
            <hr>
            <button id="saveSettings" style="margin: 1rem auto;" type="button" onclick="callSettings('hide')">Close</button>
        </div>
    </div>
    <?php require_once 'incl/profile_item_info.php'; ?>
</body>