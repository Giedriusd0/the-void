<?php 
session_start();
// Check if logged in
if(!isset($_SESSION['admin'])){
  header('Location: ../admin.php');
}
require_once 'incl/header.php';
require_once 'incl/navigation.php'; ?>

  <div id="bodyArmourContent" class="content">
    <h1>Body Armour Editor</h1>
    <table id="body_armourTable" class="contentTable">
    </table>
    <h2>Add new body armour</h2>
    <table class="addTable">
        <tr>
            <td>Name: <input type="text" id="bodyArmourNameInput"></td>
            <td>Defense: <input type="text" id="bodyArmourDefenseInput"></td>
            <td><button onclick="CreateBodyArmour()"></button></td>
        </tr>
        <tr>
          <td colspan="3" style="text-align: center"><span id="bodyArmourResponse"></span></td>
        </tr>
    </table>
  </div>
</body>
</html>