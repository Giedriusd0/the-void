<?php
session_start();
require_once '../../config.php';
require_once ROOT_FOLDER . '/sql.php';

if(isset($_POST['admin_username']) && isset($_POST['admin_password'])){
    $account = SQL::check_admin_password($_POST['admin_username'], $_POST['admin_password']);
    switch($account){
        case 'name': echo 'Invalid username.'; break;
        case 'pass': echo 'Invalid password.'; break;
        default: $_SESSION['admin'] = $_POST['admin_username']; echo true;
    }
}