<?php
require_once '../../config.php';
require_once ROOT_FOLDER . '/sql.php';

if(isset($_POST['id'])){
    switch($_POST['action'] ?? 'none'){
        case 'delete_account': {
            SQL::delete_account($_POST['id']);
            echo 'Account deleted.';
            break;
        }
        case 'delete_bodyArmour': {
            SQL::delete_body_armour($_POST['id']);
            echo 'Body armour deleted.';
            break;
        }
        case 'delete_headArmour': {
            SQL::delete_head_armour($_POST['id']);
            echo 'Head armour deleted.';
            break;
        }
        case 'delete_legsArmour': {
            SQL::delete_leg_armour($_POST['id']);
            echo 'Legs armour deleted.';
            break;
        }
        case 'delete_monsterLoot': {
            SQL::delete_monster_loot($_POST['id']);
            echo 'Monster loot deleted.';
            break;
        }
        case 'delete_monster': {
            SQL::delete_monster($_POST['id']);
            echo 'Monster deleted.';
            break;
        }
        case 'delete_shield': {
            SQL::delete_shield($_POST['id']);
            echo 'Shield deleted.';
            break;
        }
        case 'delete_weapon': {
            SQL::delete_weapon($_POST['id']);
            echo 'Weapon deleted.';
            break;
        }
        case 'delete_administrator': {
            SQL::delete_admin($_POST['id']);
            echo 'Administrator deleted.';
            break;
        }
        default: echo 'Invalid action.';
    }
} else {
    echo 'ID not set.';
}