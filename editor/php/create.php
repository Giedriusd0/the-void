<?php
require_once '../../config.php';
require_once ROOT_FOLDER . '/sql.php';

switch ($_POST['action'] ?? 'none'){
    case 'createAccount': { // Create User
        if(isset($_POST['usernameInput']) && isset($_POST['passwordInput']) && isset($_POST['emailInput'])){
            SQL::create_account($_POST['usernameInput'], $_POST['passwordInput'], $_POST['emailInput']);
            echo 'Account created.';
        } else {
            echo 'Bad inputs.';
        }
        break;
    }
    case 'createAdmin': {
        if(isset($_POST['adminUsername']) && isset($_POST['adminPassword'])){
            SQL::create_admin($_POST['adminUsername'], $_POST['adminPassword']);
            echo 'Administrator created.';
        } else {
            echo 'Bad inputs.';
        }
        break;
    }
    case 'createMonster': { // Create Monster
        if(isset($_POST['monsterNameInput']) && isset($_POST['monsterAttackInput']) && isset($_POST['monsterHealthInput']) && isset($_POST['monsterDefenseInput']) && isset($_POST['monsterLevelInput']) && isset($_POST['monsterZoneInput']) && isset($_POST['monsterExperienceInput']) && isset($_POST['monsterGoldInput'])){
            SQL::create_monster($_POST['monsterNameInput'], $_POST['monsterLevelInput'], $_POST['monsterZoneInput'], $_POST['monsterAttackInput'], $_POST['monsterHealthInput'], $_POST['monsterDefenseInput'], $_POST['monsterExperienceInput'], $_POST['monsterGoldInput']);
            echo 'Monster created.';
        } else {
            echo 'Bad inputs.';
        }
        break;
    }
    case 'createMonstersLoot': { // Create Monsters Loot
        if(isset($_POST['monsterLootId']) && isset($_POST['monsterLootItemId']) && isset($_POST['monsterLootChance'])){
            SQL::create_monster_loot($_POST['monsterLootId'], $_POST['monsterLootItemId'], $_POST['monsterLootChance']);
            echo 'Monster loot created.';
        } else {
            echo 'Bad inputs.';
        }
        break;
    }
    case 'createWeapon': { // Create Weapon
        if(isset($_POST['weaponNameInput']) && isset($_POST['weaponMinDamageInput']) && isset($_POST['weaponMaxDamageInput']) && isset($_POST['weaponAttackInput'])){
            SQL::create_weapon($_POST['weaponNameInput'], $_POST['weaponMinDamageInput'], $_POST['weaponMaxDamageInput'], $_POST['weaponAttackInput']);
            echo 'Weapon created.';
        } else {
            echo 'Bad inputs.';
        }
        break;
    }
    case 'createShield': { // Create Shield
        if(isset($_POST['shieldNameInput']) && isset($_POST['shieldDefenseInput'])){
            SQL::create_shield($_POST['shieldNameInput'], $_POST['shieldDefenseInput']);
            echo 'Shield created.';
        } else {
            echo 'Bad inputs.';
        }
        break;
    }
    case 'createHeadArmour': { // Create Head Armour
        if(isset($_POST['headArmourNameInput']) && isset($_POST['headArmourDefenseInput'])){
            SQL::create_head_armour($_POST['headArmourNameInput'], $_POST['headArmourDefenseInput']);
            echo 'Head armour created.';
        } else {
            echo 'Bad inputs.';
        }
        break;
    }
    case 'createBodyArmour': { // Create Body Armour
        if(isset($_POST['bodyArmourNameInput']) && isset($_POST['bodyArmourDefenseInput'])){
            SQL::create_body_armour($_POST['bodyArmourNameInput'], $_POST['bodyArmourDefenseInput']);
            echo 'Body armour created.';
        } else {
            echo 'Bad inputs.';
        }
        break;
    }
    case 'createLegsArmour': { // Create Legs Armour
        if(isset($_POST['legsArmourNameInput']) && isset($_POST['legsArmourDefenseInput'])){
            SQL::create_leg_armour($_POST['legsArmourNameInput'], $_POST['legsArmourDefenseInput']);
            echo 'Legs armour created.';
        } else {
            echo 'Bad inputs.';
        }
        break;
    }
    default: { // Unkown action
        echo 'Unkown action.';
    }
}
?>