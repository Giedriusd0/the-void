<?php 
session_start();
// Check if logged in
if(!isset($_SESSION['admin'])){
  header('Location: ../admin.php');
}
require_once 'incl/header.php';
require_once 'incl/navigation.php'; ?>

<div id="shieldsContent" class="content">
  <h1>Shield Editor</h1>
  <table id="shieldsTable" class="contentTable">
  </table>
  <h2>Add a new shield</h2>
  <table class="addTable">
      <tr>
        <td>Name: <input type="text" id="shieldNameInput"></td>
        <td>Defense: <input type="text" id="shieldDefenseInput"></td>
        <td><button onclick="CreateShield()"></button></td>
      </tr>
      <tr>
        <td colspan="3" style="text-align: center"><span id="shieldResponse"></span></td>
  </table>
</div>
</body>
</html>