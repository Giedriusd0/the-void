<?php 
session_start();
// Check if logged in
if(!isset($_SESSION['admin'])){
  header('Location: ../admin.php');
}
require_once 'incl/header.php';
require_once 'incl/navigation.php'; ?>

    <div id="accountsContent" class="content">
    <h1>Account Editor</h1>
    <table id="accountTable" class="contentTable">
    </table>
    <h2>Add a new account</h2>
    <table class="addTable">
        <tr>
        <td>Username: <input type="text" id="accountUsernameInput"></td>
        <td>Password: <input type="text" id="accountPasswordInput"></td>
        <td>Email: <input type="text" id="accountEmailInput"></td>
        <td><button onclick="CreateAccount()"></button></td>
        </tr>
        <tr>
        <td colspan="4" style="text-align: center"><span id="accountResponse"></span></td>
        </tr>
    </table>
    </div>
</body>
</html>