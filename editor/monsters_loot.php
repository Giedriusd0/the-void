<?php 
session_start();
// Check if logged in
if(!isset($_SESSION['admin'])){
  header('Location: ../admin.php');
}
require_once 'incl/header.php';
require_once 'incl/navigation.php'; ?>

  <div id="monstersLootContent" class="content">
    <h1>Monsters Loot</h1>
    <table id="monsters_lootTable" class="contentTable">
    </table>
    <h2>Add a new monsters loot</h2>
    <table class="addTable">
      <tr>
        <td>Monsters id: <input type="text" id="monsterLootIdInput"></td>
        <td>Item id: <input text="text" id="monsterLootItemIdInput"></td>
        <td>Chance: <input type="text" id="monsterLootChanceInput"></td>
        <td><button onclick="CreateMonsterLoot()"></button></td>
      </tr>
      <tr>
        <td colspan="4" style="text-align: center"><span id="monsterLootResponse"></span></td>
      </tr>
    </table>
  </div>
</body>
</html>