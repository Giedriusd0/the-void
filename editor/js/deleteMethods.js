function DeleteAccount(id){
    const vars = "id=" + id + "&action=delete_account";
    CreateRequest('account', 'accountResponse', vars, 'delete');
}
function DeleteAdministrator(id){
    const vars = "id=" + id + "&action=delete_administrator";
    CreateRequest('admins', 'adminResponse', vars, 'delete');
}
function DeleteBodyArmour(id){
    const vars = "id=" + id + "&action=delete_bodyArmour";
    CreateRequest('body_armour', 'bodyArmourResponse', vars, 'delete');
}
function DeleteHeadArmour(id){
    const vars = "id=" + id + "&action=delete_headArmour";
    CreateRequest('head_armour', 'headArmourResponse', vars, 'delete');
}
function DeleteLegsArmour(id){
    const vars = "id=" + id + "&action=delete_legsArmour";
    CreateRequest('leg_armour', 'legsArmourResponse', vars, 'delete');
}
function DeleteMonsterLoot(id){
    const vars = "id=" + id + "&action=delete_monsterLoot";
    CreateRequest('monsters_loot', 'monsterLootResponse', vars, 'delete');
}
function DeleteMonster(id){
    const vars = "id=" + id + "&action=delete_monster";
    CreateRequest('monsters', 'monsterResponse', vars, 'delete');
}
function DeleteShield(id){
    const vars = "id=" + id + "&action=delete_shield";
    CreateRequest('shields', 'shieldResponse', vars, 'delete');
}
function DeleteWeapon(id){
    const vars = "id=" + id + "&action=delete_weapon";
    CreateRequest('weapons', 'weaponResponse', vars, 'delete');
}