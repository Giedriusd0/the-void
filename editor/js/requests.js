function CreateRequest(TableID, ResponseID, Variables, Method){
    // Send a request to target url to add a new object to database
    // TableID - Table id which should be refreshed on request success
    // ResponseID - Id of an element, where to send response to.
    // Variables - variables which to send to target
    // Method - PHP for create, delete, modify
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200){
            if(TableID instanceof Array){
                for(var i = 0; i < TableID.length; i++){
                    RefreshTable(TableID[i]);
                }
            } else {
                RefreshTable(TableID);
            }
            document.getElementById(ResponseID).innerHTML = this.response;
            setTimeout(function() {
                document.getElementById(ResponseID).innerHTML = '';
            }, 3000);
        } 
    };
    xhttp.open('POST', 'php/' + Method + '.php', true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(Variables);
}
function RefreshTable(name){
    // Send a request to get renewed table
    if(name == null || name == '') {
        name = location.href.split("/").slice(-1).toString().slice(0, -4);
    }
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200){
            document.getElementById(name + 'Table').innerHTML = this.response;
        }
    }
    xhttp.open("GET", "incl/tables/" + name + ".php", true);
    xhttp.send();
}