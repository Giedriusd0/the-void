function CreateAccount(){
    const usernameInput = document.getElementById('accountUsernameInput').value.trim();
    const passwordInput = document.getElementById('accountPasswordInput').value.trim();
    const emailInput = document.getElementById('accountEmailInput').value.trim();
    const vars = "usernameInput=" + usernameInput + "&passwordInput=" + passwordInput + "&emailInput=" + emailInput + "&action=createAccount";

    document.getElementById('accountResponse').value = "Creating...";
    CreateRequest('account', 'accountResponse', vars, 'create');
}
function CreateMonster(){
    const nameInput = document.getElementById('monsterNameInput').value.trim();
    const levelInput = document.getElementById('monsterLevelInput').value.trim();
    const zoneInput = document.getElementById('monsterZoneInput').options[document.getElementById('monsterZoneInput').selectedIndex].text;
    const attackInput = document.getElementById('monsterAttackInput').value.trim();
    const healthInput = document.getElementById('monsterHealthInput').value.trim();
    const defenseInput = document.getElementById('monsterDefenseInput').value.trim();
    const experienceInput = document.getElementById('monsterExperienceInput').value.trim();
    const goldInput = document.getElementById('monsterGoldInput').value.trim();
    const vars = "monsterNameInput=" + nameInput + "&monsterAttackInput=" + attackInput + "&monsterHealthInput=" + healthInput + "&monsterDefenseInput=" + defenseInput + "&monsterLevelInput=" + levelInput + "&monsterZoneInput=" + zoneInput +  "&action=createMonster" + "&monsterExperienceInput=" + experienceInput + "&monsterGoldInput=" + goldInput;

    document.getElementById('monsterResponse').value = "Creating...";
    CreateRequest('monsters', 'monsterResponse', vars, 'create');
}
function CreateMonsterLoot(){
    const MonsterLootIdInput = document.getElementById('monsterLootIdInput').value.trim();
    const MonsterLootItemIdInput = document.getElementById('monsterLootItemIdInput').value.trim();
    const MonsterLootChanceInput = document.getElementById('monsterLootChanceInput').value.trim();
    const vars = "monsterLootId=" + MonsterLootIdInput + "&monsterLootItemId=" + MonsterLootItemIdInput + "&monsterLootChance=" + MonsterLootChanceInput + "&action=createMonstersLoot";

    document.getElementById('monsterLootResponse').value = "Creating...";
    CreateRequest('monsters_loot', 'monsterLootResponse', vars, 'create');
} 
function CreateWeapon(){
    const nameInput = document.getElementById('weaponNameInput').value.trim();
    const minDamageInput = document.getElementById('weaponMinDamageInput').value.trim();
    const maxDamageInput = document.getElementById('weaponMaxDamageInput').value.trim();
    const attackInput = document.getElementById('weaponAttackInput').value.trim();
    const vars = "weaponNameInput=" + nameInput + "&weaponMinDamageInput=" + minDamageInput + "&weaponMaxDamageInput=" + maxDamageInput + "&weaponAttackInput=" + attackInput + "&action=createWeapon";

    document.getElementById('weaponResponse').value = "Creating...";
    CreateRequest('weapons', 'weaponResponse', vars, 'create');
}
function CreateShield(){
    const nameInput = document.getElementById('shieldNameInput').value.trim();
    const defenseInput = document.getElementById('shieldDefenseInput').value.trim();
    const vars = "shieldNameInput=" + nameInput + "&shieldDefenseInput=" + defenseInput + "&action=createShield";

    document.getElementById('shieldResponse').value = "Creating...";
    CreateRequest('shields', 'shieldResponse', vars, 'create');
}
function CreateHeadArmour(){
    const nameInput = document.getElementById('headArmourNameInput').value.trim();
    const defenseInput = document.getElementById('headArmourDefenseInput').value.trim();
    const vars = "headArmourNameInput=" + nameInput + "&headArmourDefenseInput=" + defenseInput + "&action=createHeadArmour";

    document.getElementById('headArmourResponse').value = "Creating...";
    CreateRequest('head_armour', 'headArmourResponse', vars, 'create');
}
function CreateBodyArmour(){
    const nameInput = document.getElementById('bodyArmourNameInput').value.trim();
    const defenseInput = document.getElementById('bodyArmourDefenseInput').value.trim();
    const vars = "bodyArmourNameInput=" + nameInput + "&bodyArmourDefenseInput=" + defenseInput + "&action=createBodyArmour";

    document.getElementById('bodyArmourResponse').value = "Creating...";
    CreateRequest('body_armour', 'bodyArmourResponse', vars, 'create');
}
function CreateLegsArmour(){
    const nameInput = document.getElementById('legsArmourNameInput').value.trim();
    const defenseInput = document.getElementById('legsArmourDefenseInput').value.trim();
    const vars = "legsArmourNameInput=" + nameInput + "&legsArmourDefenseInput=" + defenseInput + "&action=createLegsArmour";

    document.getElementById('legsArmourResponse').value = "Creating...";
    CreateRequest('leg_armour', 'legsArmourResponse', vars, 'create');
}
function CreateAdmin(){
    const usernameInput = document.getElementById('adminUsernameInput').value.trim();
    const passwordInput = document.getElementById('adminPasswordInput').value.trim();
    const vars = "adminUsername=" + usernameInput + "&adminPassword=" + passwordInput + "&action=createAdmin";

    document.getElementById('adminResponse').innerHTML = "Creating...";
    CreateRequest('admins', 'adminResponse', vars, 'create');
}