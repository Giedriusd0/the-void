function LogIn(){
    const Username = document.getElementById("usernameInput").value.trim();
    const Password = document.getElementById("passwordInput").value.trim();
    const Response = document.getElementById("response");
    const Variables = "admin_username=" + Username + "&admin_password=" + Password;

    if(Username != ""){
        if(Password != ""){
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function(){
                if(this.readyState == 4 && this.status == 200){
                    if(this.response == true){
                        window.location.href = 'editor/account.php';
                    } else {
                        Response.innerHTML = this.response;
                        setTimeout(function() {
                            Response.innerHTML = '';
                        }, 3000);
                    }
                }
            }
            // Send
            xhttp.open('POST', 'editor/php/login.php', true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send(Variables);
        } else {
            Response.innerHTML = "Please type in the password.";
            setTimeout(function(){
                Response.innerHTML = "";
            }, 3000);
        }
    } else {
        Response.innerHTML = "Please type in the username.";
        setTimeout(function(){
            Response.innerHTML = "";
        }, 3000);
    }
}