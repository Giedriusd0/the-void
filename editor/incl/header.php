<!DOCTYPE html>
<html lang="lt">
<head>
    <meta charset="UTF-8">
    <title>Skylight Engine</title>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="css/reset.css" rel="stylesheet" type="text/css">
    <link href="css/header.css" rel="stylesheet" type="text/css">
    <link href="css/navigation.css" rel="stylesheet" type="text/css">
    <link href="css/tables.css" rel="stylesheet" type="text/css">
    <script src="js/requests.js"></script>
    <script src="js/createMethods.js"></script>
    <script src="js/deleteMethods.js"></script>
</head>
<body onload="RefreshTable()">
    <div id="header">
        <p>Skylight Engine</p>
    </div>