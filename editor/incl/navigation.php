    <div id="navigationBar">
        <!-- ........ Account ........... -->
        <div class="dropdown">
            <button class="dropbtn">Account<i class="fa fa-angle-double-down"></i></button>
            <div class="dropdown-content">
                <a class="dropContbtn" href="account.php">General</a>
                <a class="dropContbtn" href="stats.php">Statistics</a>
                <a class="dropContbtn" href="inventory.php">Inventory</a>
                <a class="dropContbtn" href="equipment.php">Equipment</a>
                <a class="dropContbtn" href="admins.php">Admins</a>
            </div>
        </div>
        <!--- ........ Monsters ........ -->
        <div class="dropdown">
            <button class="dropbtn">Monsters <i class="fa fa-angle-double-down"></i></button>
            <div class="dropdown-content">
                <a class="dropContbtn" href="monsters.php">Monsters</a>
                <a class="dropContbtn" href="monsters_loot.php">Monster Loot</a>
            </div>
        </div>
        <!--- ........ Items ........... -->
        <div class="dropdown">
            <button class="dropbtn">Items<i class="fa fa-angle-double-down"></i></button>
            <div class="dropdown-content">
                <a class="dropContbtn" href="weapons.php">Weapons</a>
                <a class="dropContbtn" href="shields.php">Shields</a>
                <a class="dropContbtn" href="head_armour.php">Head Armour</a>
                <a class="dropContbtn" href="body_armour.php">Body Armour</a>
                <a class="dropContbtn" href="leg_armour.php">Leg Armour</a>
            </div>
        </div>
        <div id="loggedIn">
            <p>Logged in as <?php echo $_SESSION['admin'] ?></p>
            <a href="php/logout.php">Log Out</a>
        </div>
    </div>