<tr>
    <th>Id</th>
    <th>Username</th>
    <th>Password</th>
    <th>Email</th>
    <th>Gold</th>
    <th>Zone</th>
    <th>Modify</th>
    <th>Delete</th>
</tr>
<?php
    require_once '../../../config.php';
    require_once ROOT_FOLDER . '/sql.php';
    
    $accounts = SQL::get_account_all();
    if(is_bool($accounts) || mysqli_num_rows($accounts) == 0){
        echo '<tr><td colspan="10" style="text-align: center">No records present.</td></tr>';
    } else {
        while($row = $accounts->fetch_assoc()){
            echo "<tr>";
            $id = 0;
            foreach($row as $key => $value){
                if($key == "id") $id = $value;
                echo "<td>$value</td>";
            }
            echo "<td class='tableButtons'><button class='editButton' onclick='ModifyAccount($id)'></button></td>";
            echo "<td class='tableButtons'><button class='removeButton' onclick='DeleteAccount($id)'></button></td>";
            echo "</tr>";
        }
    }
?>