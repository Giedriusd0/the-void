<tr>
    <th>Id</th>
    <th>Name</th>
    <th>Defense</th>
    <th>Image</th>
    <th>Modify</th>
    <th>Delete</th>
</tr>
<?php 
    require_once '../../../config.php';
    require_once ROOT_FOLDER . '/sql.php';
    $shields = SQL::get_shield_all();
    if(is_bool($shields) || mysqli_num_rows($shields) == 0){
        echo '<tr><td colspan="10" style="text-align: center">No records present.</td></tr>';
    } else {
        while($row = $shields->fetch_assoc()){
            echo "<tr>";
            $id = 0;
            foreach($row as $key => $value){
                if($key == 'id') $id = $value;
                echo "<td>$value</td>";
            }
            if(file_exists("../img/shield/$id.jpg"))
                echo "<td><a href='img/shield/$id.jpg'>Exists</a></td>";
            else 
                echo "<td>Not found</td>";
                echo "<td class='tableButtons'><button class='editButton' onclick='ModifyShield($id)'></button></td>";
                echo "<td class='tableButtons'><button class='removeButton' onclick='DeleteShield($id)'></button></td>";
            echo "</tr>";
        }
    }
?>