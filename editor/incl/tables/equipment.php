<tr>
    <th>Id</th>
    <th>Account Id</th>
    <th>Head</th>
    <th>Body</th>
    <th>Legs</th>
    <th>Left Hand</th>
    <th>Right Hand</th>
    <th>Modify</th>
</tr>
<?php
    require_once '../../../config.php';
    require_once ROOT_FOLDER . '/sql.php';
    $equipment = SQL::get_equipment_all();
    if(is_bool($equipment) || mysqli_num_rows($equipment) == 0){
        echo '<tr><td colspan="8" style="text-align: center">No records present.</td></tr>';
    } else {
        while($row = $equipment->fetch_assoc()){
            echo "<tr>";
            $id = 0;
            foreach($row as $key => $value){
                if($key == 'id') $id = $value;
                echo "<td>$value</td>";
            }
            echo "<td class='tableButtons'><button class='editButton' onclick='ModifyEquipment($id)'></button></td>";
            echo "</tr>";
        }
    }
?>
