<tr>
    <th>Id</th>
    <th>Name</th>
    <th>Level</th>
    <th>Zone</th>
    <th>Attack</th>
    <th>Health</th>
    <th>Defense</th>
    <th>Experience</th>
    <th>Gold</th>
    <th>Image</th>
    <th>Modify</th>
    <th>Delete</th>
</tr>
<?php
    require_once '../../../config.php';
    require_once ROOT_FOLDER . '/sql.php';
    $monsters = SQL::get_monster_all();
    if(is_bool($monsters) || mysqli_num_rows($monsters) == 0){
        echo '<tr><td colspan="12" style="text-align: center">No records present.</td></tr>';
    } else {
        while($rows = $monsters -> fetch_assoc()){
            echo '<tr>';
            $id = 0;
            foreach($rows as $key => $value){
                if($key == 'id') $id = $value;
                    echo "<td>$value</td>";
            }
            if(file_exists("../img/monster/$id.jpg"))
                echo "<td><a href='img/monster/$id.jpg'>Exists</a></td>";
            else 
                echo "<td>Not found</td>";
                echo "<td class='tableButtons'><button class='editButton'  onclick='ModifyMonster($id)'></button></td>";
                echo "<td class='tableButtons'><button class='removeButton' onclick='DeleteMonster($id)'></button></td>";
            echo '</tr>';
        }
    }
?>