<tr>
    <th>Id</th>
    <th>Monsters Id</th>
    <th>Item Id</th>
    <th>Chance</th>
    <th>Modify</th>
    <th>Delete</th>
</tr>
<?php
    require_once '../../../config.php';
    require_once ROOT_FOLDER . '/sql.php';
    $loot = SQL::get_monster_loot_all();
    if(is_bool($loot) || mysqli_num_rows($loot) == 0){
        echo '<tr><td colspan="6" style="text-align: center">No records present.</td></tr>';
    } else {
        while($rows = $loot -> fetch_assoc()){
            echo '<tr>';
            $id = 0;
            foreach($rows as $key => $value){
                if($key == 'id') $id = $value;
                echo "<td>$value</td>";
            }
            echo "<td class='tableButtons'><button class='editButton'  onclick='ModifyMonsterLoot($id)'></button></td>";
            echo "<td class='tableButtons'><button class='removeButton' onclick='DeleteMonsterLoot($id)'></button></td>";
            echo '</tr>';
        }
    }