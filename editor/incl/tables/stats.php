<tr>
    <th>Id</th>
    <th>Account Id</th>
    <th>Attack</th>
    <th>Strength</th>
    <th>Health</th>
    <th>Defense</th>
    <th>Agility</th>
    <th>Experience</th>
    <th>Free Points</th>
    <th>Modify</th>
</tr>
<?php
    require_once '../../../config.php';
    require_once ROOT_FOLDER . '/sql.php';
    $accounts = SQL::get_stats_all();
    if(is_bool($accounts) || mysqli_num_rows($accounts) == 0){
        echo '<tr><td colspan="10" style="text-align: center">No records present.</td></tr>';
    } else {
        while($row = $accounts->fetch_assoc()){
            echo "<tr>";
            foreach($row as $key => $value){
                if($key == "id") $id = $value;
                echo "<td>$value</td>";
            }
            echo "<td class='tableButtons'><button class='editButton' onclick='ModifyStats($id)'></button></td>";
            echo "</tr>";
        }
    }
?>