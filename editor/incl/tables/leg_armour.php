<tr>
    <th>Id</th>
    <th>Name</th>
    <th>Defense</th>
    <th>Image</th>
    <th>Modify</th>
    <th>Delete</th>
</tr>
<?php 
    require_once '../../../config.php';
    require_once ROOT_FOLDER . '/sql.php';
    $legsArmour = SQL::get_leg_armour_all();
    if(is_bool($legsArmour) || mysqli_num_rows($legsArmour) == 0){
        echo '<tr><td colspan="10" style="text-align: center">No records present.</td></tr>';
    } else {
        while($row = $legsArmour->fetch_assoc()){
            echo "<tr>";
            $id = 0;
            foreach($row as $key => $value){
                if($key == 'id') $id = $value;
                echo "<td>$value</td>";
            }
            if(file_exists("../img/legsArmour/$id.jpg"))
                echo "<td><a href='img/legsArmour/$id.jpg'>Exists</a></td>";
            else 
                echo "<td>Not found</td>";
            echo "<td class='tableButtons'><button class='editButton'  onclick='ModifyLegsArmour($id)'></button></td>";
            echo "<td class='tableButtons'><button class='removeButton' onclick='DeleteLegsArmour($id)'></button></td>";
            echo "</tr>";
        }
    }
?>