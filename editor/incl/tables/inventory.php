<tr>
    <th>Id</th>
    <th>Account Id</th>
    <th>Slot0</th>
    <th>Slot1</th>
    <th>Slot2</th>
    <th>Slot3</th>
    <th>Slot4</th>
    <th>Slot5</th>
    <th>Slot6</th>
    <th>Slot7</th>
    <th>Modify</th>
</tr>
<?php
    require_once '../../../config.php';
    require_once ROOT_FOLDER . '/sql.php';
    $accounts = SQL::get_inventory_all();
    if(is_bool($accounts) || mysqli_num_rows($accounts) == 0){
        echo '<tr><td colspan="10" style="text-align: center">No records present.</td></tr>';
    } else {
        while($row = $accounts->fetch_assoc()){
            echo "<tr>";
            $id = 0;
            foreach($row as $key => $value){
                if($key == "id") $id = $value;
                echo "<td>$value</td>";
            }
            echo "<td class='tableButtons'><button class='editButton' onclick='ModifyInventory($id)'></button></td>";
            echo "</tr>";
        }
    }
?>