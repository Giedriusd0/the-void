<tr>
    <th>Id</th>
    <th>Name</th>
    <th>Defense</th>
    <th>Image</th>
    <th>Modify</th>
    <th>Delete</th>
</tr>
<?php
    require_once '../../../config.php';
    require_once ROOT_FOLDER . '/sql.php';
    $bodyArmour = SQL::get_body_armour_all();
    if(is_bool($bodyArmour) || mysqli_num_rows($bodyArmour) == 0){
        echo '<tr><td colspan="6" style="text-align: center">No records present.</td></tr>';
    } else {
        while($row = $bodyArmour->fetch_assoc()){
            echo "<tr>";
            $id = 0;
            foreach($row as $key => $value){
                if($key == 'id') $id = $value;
                echo "<td>$value</td>";
            }
            if(file_exists("../img/bodyArmour/$id.jpg"))
                echo "<td><a href='img/bodyArmour/$id.jpg'>Exists</a></td>";
            else 
                echo "<td>Not found</td>";
                echo "<td class='tableButtons'><button class='editButton' onclick='ModifyBodyArmour($id)'></button></td>";
                echo "<td class='tableButtons'><button class='removeButton' onclick='DeleteBodyArmour($id)'></button></td>";
            echo "</tr>";
        }
    }
?>
