<tr>
    <th>Id</th>
    <th>Name</th>
    <th>Min Damage</th>
    <th>Max Damage</th>
    <th>Attack</th>
    <th>Image</th>
    <th>Modify</th>
    <th>Delete</th>
</tr>
<?php
    require_once '../../../config.php';
    require_once ROOT_FOLDER . '/sql.php';
    $weapons = SQL::get_weapon_all();
    if(is_bool($weapons) || mysqli_num_rows($weapons) == 0){
        echo '<tr><td colspan="8" style="text-align: center">No records present.</td></tr>';
    } else {
        while($row = $weapons->fetch_assoc()){
        echo "<tr>";
        $id = 0;
        foreach($row as $key => $value){
            if($key == 'id') $id = $value;
            echo "<td>$value</td>";
            }
        if(file_exists("../img/weapon/$id.jpg"))
            echo "<td><a href='img/weapon/$id.jpg'>Exists</a></td>";
        else 
            echo "<td>Not found</td>";
            echo "<td class='tableButtons'><button class='editButton' onclick='ModifyWeapon($id)'></button></td>";
            echo "<td class='tableButtons'><button class='removeButton' onclick='DeleteWeapon($id)'></button></td>";
        echo "</tr>";
        }
    }
?>