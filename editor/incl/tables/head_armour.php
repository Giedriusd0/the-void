<tr>
    <th>Id</th>
    <th>Name</th>
    <th>Defense</th>
    <th>Image</th>
    <th>Modify</th>
    <th>Delete</th>
</tr>
<?php
    require_once '../../../config.php';
    require_once ROOT_FOLDER . '/sql.php';
    $headArmours = SQL::get_head_armour_all();
    if(is_bool($headArmours) || mysqli_num_rows($headArmours) == 0){
        echo '<tr><td colspan="6" style="text-align: center">No records present.</td></tr>';
    } else {
        while($row = $headArmours->fetch_assoc()){
            echo "<tr>";
            $id = 0;
            foreach($row as $key => $value){
                if($key == 'id') $id = $value;
                echo "<td>$value</td>";
            }
            if(file_exists("../img/headArmour/$id.jpg"))
                echo "<td><a href='img/headArmour/$id.jpg'>Exists</a></td>";
            else 
                echo "<td>Not found</td>";
            echo "<td class='tableButtons'><button class='editButton' onclick='ModifyHeadArmour($id)'></button></td>";
            echo "<td class='tableButtons'><button class='removeButton' onclick='DeleteHeadArmour($id)'></button></td>";
            echo "</tr>";
        }
    }
?>