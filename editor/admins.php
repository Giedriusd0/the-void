<?php 
session_start();
// Check if logged in
if(!isset($_SESSION['admin'])){
  header('Location: ../admin.php');
}
require_once 'incl/header.php';
require_once 'incl/navigation.php'; ?>

  <div id="administratorsContent" class="content">
    <h1>Administrators Editor</h1>
    <table id="adminsTable" class="contentTable">
    </table>
    <h2>Add a new administrator</h2>
    <table class="addTable">
      <tr>
        <td>Username: <input type="text" id="adminUsernameInput"></td>
        <td>Password: <input type="text" id="adminPasswordInput"></td>
        <td><button onclick="CreateAdmin()"></button></td>
      </tr>
      <tr>
        <td colspan="3" style="text-align: center"><span id="adminResponse"></span></td>
      </tr>
    </table>
  </div>
</body>
</html>