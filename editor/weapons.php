<?php 
session_start();
// Check if logged in
if(!isset($_SESSION['admin'])){
  header('Location: ../admin.php');
}
require_once 'incl/header.php';
require_once 'incl/navigation.php'; ?>

  <div id="weaponsContent" class="content">
    <h1>Weapon Editor</h1>
    <table id="weaponsTable" class="contentTable">
    </table>
    <h2>Add a new weapon</h2>
    <table class="addTable">
      <tr>
        <td>Name: <input type="text" id="weaponNameInput"></td>
        <td>Min Damage: <input type="text" id="weaponMinDamageInput"></td>
        <td>Max Damage: <input type="text" id="weaponMaxDamageInput"></td>
        <td>Attack: <input type="text" id="weaponAttackInput"></td>
        <td><button onclick="CreateWeapon()"></button></td>
      </tr>
      <tr>
        <td colspan="5" style="text-align: center"><span id="weaponResponse"></span></td>
    </table>
  </div>
</body>
</html>