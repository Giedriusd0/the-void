<?php 
session_start();
// Check if logged in
if(!isset($_SESSION['admin'])){
  header('Location: ../admin.php');
}
require_once 'incl/header.php';
require_once 'incl/navigation.php'; ?>

  <div id="monstersContent" class="content">
    <h1>Monster Editor</h1>
    <table id="monstersTable" class="contentTable">
    </table>
    <h2>Add a new monster</h2>
    <table class="addTable">
      <tr>
        <td>Name: <input type="text" id="monsterNameInput"></td>
        <td>Zone: 
          <select id="monsterZoneInput">
            <option value="Forest">Forest</option>
            <option value="Underground">Underground</option>
            <option value="Lakes">Lakes</option>
            <option value="MistyRuins">Misty Ruins</option>
          </select>
        </td>            
        <td>Level: <input type="text" id="monsterLevelInput"></td>
        <td>Attack: <input type="text" id="monsterAttackInput"></td>
        <td>Health: <input type="text" id="monsterHealthInput"></td>
        <td>Defense: <input type="text" id="monsterDefenseInput"></td>
        <td>Experience: <input type="text" id="monsterExperienceInput"></td>
        <td>Gold: <input type="text" id="monsterGoldInput"></td>
        <td><button onclick="CreateMonster()"></button></td>
      </tr>
      <tr>
        <td colspan="7" style="text-align: center"><span id="monsterResponse"></span></td>
    </table>
  </div>
</body>
</html>