<?php 
session_start();
// Check if logged in
if(!isset($_SESSION['admin'])){
  header('Location: ../admin.php');
}
require_once 'incl/header.php';
require_once 'incl/navigation.php'; ?>

  <div id="statsContent" class="content">
    <h1>Statistics Editor</h1>
    <table id="statsTable" class="contentTable">
    </table>
  </div>
</body>
</html>