<?php 
session_start();
// Check if logged in
if(!isset($_SESSION['admin'])){
  header('Location: ../admin.php');
}
require_once 'incl/header.php';
require_once 'incl/navigation.php'; ?>

  <div id="headArmourContent" class="content">
    <h1>Head Armour Editor</h1>
    <table id="head_armourTable" class="contentTable">
    </table>
    <h2>Add new head armour</h2>
    <table class="addTable">
        <tr>
            <td>Name: <input type="text" id="headArmourNameInput"></td>
            <td>Defense: <input type="text" id="headArmourDefenseInput"></td>
            <td><button onclick="CreateHeadArmour()"></button></td>
        </tr>
        <tr>
          <td colspan="3" style="text-align: center"><span id="headArmourResponse"></span></td>
        </tr>
    </table>
  </div>
</body>
</html>