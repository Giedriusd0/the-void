<?php 
session_start();
// Check if logged in
if(!isset($_SESSION['admin'])){
  header('Location: ../admin.php');
}
require_once 'incl/header.php';
require_once 'incl/navigation.php'; ?>

  <div id="inventoryContent"  class="content">
    <h1>Inventory Editor</h1>
    <table id="inventoryTable" class="contentTable">
    </table>
  </div>
</body>
</html>