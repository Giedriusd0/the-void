<?php 
session_start();
// Check if logged in
if(!isset($_SESSION['admin'])){
  header('Location: ../admin.php');
}
require_once 'incl/header.php';
require_once 'incl/navigation.php'; ?>

  <div id="legsArmourContent" class="content">
    <h1>Legs Armour Editor</h1>
    <table id="leg_armourTable" class="contentTable">
    </table>
    <h2>Add new legs armour</h2>
    <table class="addTable">
      <tr>
        <td>Name: <input type="text" id="legsArmourNameInput"></td>
        <td>Defense: <input type="text" id="legsArmourDefenseInput"></td>
        <td><button onclick="CreateLegsArmour()"></button></td>
      </tr>
      <tr>
        <td colspan="3" style="text-align: center"><span id="legsArmourResponse"></span></td>
      </tr>
    </table>
  </div>
</body>
</html>