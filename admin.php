<!DOCTYPE html>
<html lang="lt">
<head>
    <meta charset="UTF-8">
    <title>Skylight Engine</title>
    <script src="editor/js/login.js"></script>
    <link href="editor/css/login.css" type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet"> 
</head>
<body>
    <div id="loginContainer">
        <h1>Skylight Engine</h1>
        <table>
            <tr>
                <td><input type="text" id="usernameInput" placeholder="Username.."></td>
            </tr>
            <tr>
                <td><input type="password" id="passwordInput" placeholder="Password.."></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" value="Log in" onclick="LogIn()"></td>
            </tr>
            <tr>
                <td colspan="2"><span id="response"></span></td>
            </tr>
        </table>
    </div>
</body>
</html>