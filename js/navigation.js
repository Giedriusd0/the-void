function changeForm(location){
    const Login = document.getElementById('loginForm');
    const Register = document.getElementById('registerForm');
    const RegisterSuccess = document.getElementById('registerFormSuccess');

    // RESET ALL
    Login.style.display = 'none';
    Register.style.display = 'none';
    RegisterSuccess.style.display = 'none';

    switch(location){
        case 'login': {
            Login.style.display = 'block'; 
            break;
        }
        case 'register': {
            Register.style.display = 'block'; 
            break;
        }
        case 'registerSuccess': {
            RegisterSuccess.style.display = 'block';
            break;
        }
        default : {
            window.alert("ERROR ON SWITCH. VALUE: " + location);
        }
    }
}