function register() {
    // Inputs
    const Username = document.getElementById("registerUsername").value.trim();
    const Password = document.getElementById("registerPassword").value.trim();
    const PasswordConfirm = document.getElementById("registerPasswordConfirm").value.trim();
    const Email = document.getElementById("registerEmail").value.trim();
    // Responses
    const UsernameResponse = document.getElementById("registerUsernameResponse");
    const PasswordResponse = document.getElementById("registerPasswordResponse");
    const EmailResponse = document.getElementById("registerEmailResponse");
    // Forms
    const RegisterForm = document.getElementById("registerForm");
    const RegisterFormSuccess = document.getElementById("registerFormSuccess");
    // Request
    const Variables = "register_username=" + Username + "&register_password=" + Password + "&register_email=" + Email + "&action=register";
    //check if email
    const emailPattern = new RegExp(/^[^\s@]+@[^\s@]+\.[^\s@]+$/);

    if(Username != ''){
        if((Password != '')&&(Password.length >= 6)){
            if (Password === PasswordConfirm){
                if((Email != '')&&(emailPattern.test(Email))){
                    var xhttp = new XMLHttpRequest();
                        xhttp.onreadystatechange = function() {
                            if(this.readyState == 4 && this.status == 200){
                                // Success
                                if(this.response == 'true'){
                                    changeForm('registerSuccess');
                                    // Reset fields
                                    document.getElementById("registerUsername").value = '';
                                    document.getElementById("registerPassword").value = '';
                                    document.getElementById("registerPasswordConfirm").value = '';
                                    document.getElementById("registerEmail").value = '';
                                } else {
                                    // Failed
                                    switch(this.response){
                                        case 'username_in_use': {
                                            UsernameResponse.innerHTML = 'Username is already taken.';
                                            setTimeout(function(){
                                                UsernameResponse.innerHTML = '';
                                            }, 3000);
                                            break;
                                        } 
                                        case 'email_in_use': {
                                            EmailResponse.innerHTML = 'Email is already taken.';
                                            setTimeout(function(){
                                                EmailResponse.innerHTML = '';
                                            }, 3000);
                                        }
                                    }
                                }
                            }
                        }
                    xhttp.open('POST', 'game/php/register.php', true);
                    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    xhttp.send(Variables);
                // No email
                } else {
                    EmailResponse.innerHTML = "Please type in your email.";
                    setTimeout(function(){
                        EmailResponse.innerHTML = '';
                    }, 3000);
                }
            // Passwords dont match
            } else {
                PasswordResponse.innerHTML = 'Passwords do not match.';
                setTimeout(function(){
                    PasswordResponse.innerHTML = '';
                }, 3000);
            }
        // Password empty
        } else {
            PasswordResponse.innerHTML = 'Please type in a pasword of at least 6 characters.';
            setTimeout(function() {
                PasswordResponse.innerHTML = '';
            }, 3000);
        }
    // Username empty
    } else {
        UsernameResponse.innerHTML = 'Please type in the username.';
        setTimeout(function() {
            UsernameResponse.innerHTML = '';
        }, 3000);
    }
}