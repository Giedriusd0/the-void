window.onload = function(){
    var passwordField = document.getElementById("loginPassword");
    var usernameField = document.getElementById("loginUsername");

    passwordField.addEventListener("keyup", function(event) {
        event.preventDefault();
        if(event.keyCode === 13){
            login();
        }
    });
    usernameField.addEventListener("keyup", function(event) {
        event.preventDefault();
        if(event.keyCode === 13){
            login();
        }
    });
}

function login(){
    // Inputs
    const Username = document.getElementById("loginUsername").value.trim();
    const Password = document.getElementById("loginPassword").value.trim();
    // Responses
    const UsernameResponse = document.getElementById("loginUsernameResponse");
    const PasswordResponse = document.getElementById("loginPasswordResponse");
    // Request
    const Variables = "account_username=" + Username + "&account_password=" + Password;

    if(Username != ''){
        if(Password != ''){
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if(this.readyState == 4 && this.status == 200){
                    // Log In Success
                    if(this.response == true){
                        window.location.href = 'game/profile.php';
                    // Log In Failed
                    } else {
                        const result = JSON.parse(this.response);
                        // Bad username
                        if(result.error == 'username'){
                            UsernameResponse.innerHTML = result.message;
                            setTimeout(function() {
                                UsernameResponse.innerHTML = '';
                            }, 3000);
                        // Bad password 
                        } else if (result.error == 'password'){
                            PasswordResponse.innerHTML = result.message;
                            setTimeout(function() {
                                PasswordResponse.innerHTML = '';
                            }, 3000);
                        } else {
                            window.alert(this.response);
                        }
                    }
                }
            }
            xhttp.open('POST', 'game/php/login.php', true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send(Variables);
        } else {
            PasswordResponse.innerHTML = 'Please type in the password.';
            setTimeout(function() {
                Response.innerHTML = '';
            }, 3000);
        }
    } else {
        UsernameResponse.innerHTML = 'Please type in the username.';
        setTimeout(function() {
            Response.innerHTML = '';
        }, 3000);
    }
}