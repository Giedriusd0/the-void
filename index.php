<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    session_start(); 
    // Check if logged in
    if(isset($_SESSION['player'])) 
        header("Location: game/profile.php");
?>    
<!DOCTYPE html>
<html lang="lt">
<head>
    <meta charset="UTF-8">
    <title>The Void</title>
    <link href="css/layout.css" rel="stylesheet" type="text/css">
    <link href="css/skin.css" rel="stylesheet" type="text/css">
    <script src="js/login.js"></script>
    <script src="js/register.js"></script>
    <script src="js/navigation.js"></script>
</head>
<body>
    <h1 id="header">The Void</h1>
    <h1 id="subheader">Do you dare to enter?</h1>
    <div id="container">        
        <!-- Navigation -->
        <button class="navigationButton" type="button" onclick="changeForm('login')">Log In</button>
        <button class="navigationButton" type="button" onclick="changeForm('register')">Register</button>
        <!-- Content -->
        <!-- Log in -->
        <div id="loginForm">
            <p>Username</p>
            <input type="text" id="loginUsername">
            <p id="loginUsernameResponse"></p>
            <p>Password</p>
            <input type="password" id="loginPassword">
            <p id="loginPasswordResponse"></p>
            <button type="button" onclick="login()">Login</button>
        </div>
        <!-- Register -->
        <div id="registerForm">
            <p>Username</p>
            <input type="text" id="registerUsername">
            <p id="registerUsernameResponse"></p>
            <p>Password</p>
            <input type="password" id="registerPassword">
            <p>Confirm Password</p>
            <input type="password" id="registerPasswordConfirm">
            <p id="registerPasswordResponse"></p>
            <p>Email</p>
            <input type="text" id="registerEmail">
            <p id="registerEmailResponse"></p>
            <button type="button" onclick="register()">Register</button>
        </div>
        <!-- Register success -->
        <div id="registerFormSuccess">
            <p>Successfully registered.</p>
            <button type="button" onclick="changeForm('login')">Log in</button>
        </div>
    </div>
</body>
</html>
