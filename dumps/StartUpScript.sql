-- -----------------------------------------------------------------------------
-- USERS
-- -----------------------------------------------------------------------------
# ADMIN 
DROP USER IF EXISTS 'main_admin'@'localhost';
CREATE USER 'main_admin'@'localhost' IDENTIFIED BY 'studentas007';
GRANT ALL PRIVILEGES ON thevoid.* TO 'main_admin'@'localhost' WITH GRANT OPTION;

# USER 
DROP USER IF EXISTS 'main_user'@'localhost';
CREATE USER 'main_user'@'localhost' IDENTIFIED BY 'stud007';
GRANT EXECUTE ON thevoid.* TO 'main_user'@'localhost';

FLUSH PRIVILEGES;

-- -----------------------------------------------------------------------------
-- TRIGGERS
-- -----------------------------------------------------------------------------
DROP TRIGGER IF EXISTS account_after_insert;
DROP TRIGGER IF EXISTS accounts_before_delete;
DROP TRIGGER IF EXISTS monsters_before_delete;

DELIMITER $$
    CREATE TRIGGER account_after_insert AFTER INSERT
    ON thevoid.account
    FOR EACH ROW BEGIN
	  INSERT INTO thevoid.inventory (accountId, slot0, slot1, slot2, slot3, slot4, slot5, slot6, slot7, equipHead, equipTorso, equipLegs, equipWeapon, equipShield) 
      VALUES (NEW.id, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');
      INSERT INTO thevoid.statistics (accountId, statLevel, statAttack, statStrength, statVitality, statDefense, statAgility, statExperience, statRemainingPoints, statGold, statCurrentZone) 
      VALUES (NEW.id, '1', '10', '10', '10', '10', '10', '0', '0', '0', '1');
    END$$

    CREATE TRIGGER accounts_before_delete 
    BEFORE DELETE 
    ON thevoid.account
    FOR EACH ROW BEGIN
	  DELETE FROM statistics WHERE accountId=OLD.id;
      DELETE FROM inventory WHERE accountId=OLD.id;
    END$$

    CREATE TRIGGER monsters_before_delete 
    BEFORE DELETE ON thevoid.monster
    FOR EACH ROW BEGIN
	  DELETE FROM monsterloot WHERE monsterId=OLD.id;
    END$$
DELIMITER ;

-- -----------------------------------------------------------------------------
-- STORED PROCEDURES
-- -----------------------------------------------------------------------------
DELIMITER $$
DROP PROCEDURE IF EXISTS create_account $$
CREATE DEFINER='main_admin'@'localhost' PROCEDURE create_account (_username VARCHAR(64), _password CHAR(255), _email VARCHAR(100)) 
	BEGIN
		IF (SELECT COUNT(id) FROM account WHERE username = _username) > 0 THEN
			SELECT 'username_in_use' AS response;
        ELSEIF (SELECT COUNT(id) FROM account WHERE email = _email) > 0 THEN
			SELECT 'email_in_use' AS response;
        ELSE
			INSERT INTO account (username, password, email) VALUES (_username, _password, _email);
            SELECT 'true' AS response;
		END IF;
    END $$
DROP PROCEDURE IF EXISTS get_account_password_by_name $$
CREATE DEFINER='main_admin'@'localhost' PROCEDURE get_account_password_by_name (_username VARCHAR(64))
	BEGIN
		IF (SELECT COUNT(id) FROM account WHERE username = _username) > 0 THEN
			SELECT id, password FROM account WHERE username = _username;
		ELSE 
			SELECT 'username_not_taken' AS response;
		END IF;
    END $$
DROP PROCEDURE IF EXISTS get_account_all $$
CREATE DEFINER ='main_admin'@'localhost' PROCEDURE get_account_all (_id INT)
	BEGIN
		SELECT
        -- Main
        account.id, account.username,
        -- Stats
        statistics.statLevel, statistics.statAttack, statistics.statStrength, statistics.statVitality, statistics.statDefense, statistics.statAgility, 
        statistics.statExperience, statistics.statRemainingPoints, statistics.statGold, statistics.statCurrentZone,
        -- Inventory
        inventory.slot0, inventory.slot1, inventory.slot2, inventory.slot3, inventory.slot4, inventory.slot5, inventory.slot6, 
        inventory.slot7, inventory.equipHead, inventory.equipTorso, inventory.equipLegs, inventory.equipWeapon, inventory.equipShield
        FROM account 
        INNER JOIN statistics ON account.id = statistics.accountId
        INNER JOIN inventory ON account.id = inventory.accountId
        WHERE account.id = _id;
    END $$
DROP PROCEDURE IF EXISTS thevoid.get_item $$
CREATE DEFINER ='main_admin'@'localhost' PROCEDURE get_item (_id SMALLINT)
	BEGIN
		SELECT * FROM item WHERE id = _id;
    END $$
DROP PROCEDURE IF EXISTS thevoid.get_item_properties $$
CREATE DEFINER ='main_admin'@'localhost' PROCEDURE get_item_properties (_id SMALLINT)
	BEGIN
		SELECT * FROM itemproperties WHERE itemId = _id;
    END $$
DROP PROCEDURE IF EXISTS thevoid.get_monster $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE get_monster (_id SMALLINT)
	BEGIN
		SELECT * FROM monster WHERE id = _id;
    END $$
DROP PROCEDURE IF EXISTS thevoid.get_monster_loot $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE get_monster_loot (_id SMALLINT)
	BEGIN 
		SELECT * FROM monsterloot WHERE monsterId = _id;
	END $$
DROP PROCEDURE IF EXISTS thevoid.get_zone $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE get_zone (_id SMALLINT)
    BEGIN 
        SELECT * FROM zone WHERE id = _id;
    END $$ 
DROP PROCEDURE IF EXISTS thevoid.update_account_email $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE update_account_email (_id INT, _email VARCHAR(100))
	BEGIN
		IF (SELECT COUNT(id) FROM account WHERE email = _email) > 0 THEN
			SELECT '1' AS response;
		ELSE 
			UPDATE account SET email = _email WHERE id = _id;
            SELECT '0' AS response;
		END IF;
	END $$
DROP PROCEDURE IF EXISTS thevoid.update_account_password $$ 
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE update_account_password (_id INT, _password CHAR(255))
	BEGIN
		UPDATE account SET password = _password WHERE id = _id;
        SELECT '0' AS response;
    END $$
DROP PROCEDURE IF EXISTS thevoid.update_account_stats $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE update_account_stats (_id INT, _attack TINYINT, _strength TINYINT, _vitality TINYINT, _defense TINYINT, _agility TINYINT, _remainingPoints TINYINT)
	BEGIN 
		UPDATE statistics SET statAttack = _attack, statStrength = _strength, statVitality = _vitality, statDefense = _defense, statAgility = _agility, statRemainingPoints = _remainingPoints WHERE accountId = _id;
        SELECT 'true' AS response;
	END $$
DROP PROCEDURE IF EXISTS thevoid.update_account_experience $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE update_account_experience (_id INT, _experience SMALLINT)
	BEGIN 
		UPDATE statistics SET statExperience = _experience WHERE accountId = _id;
        SELECT 'true' AS response;
    END $$
DROP PROCEDURE IF EXISTS thevoid.update_account_remainingPoints $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE update_account_remainingPoints (_id INT, _remainingPoints TINYINT)
	BEGIN
		UPDATE statistics SET statRemainingPoints = _remainingPoints WHERE accountId = _id;
    END$$
DROP PROCEDURE IF EXISTS thevoid.update_account_currentZone $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE update_account_currentZone (_id INT, _zone TINYINT)
	BEGIN
		UPDATE statistics SET statCurrentZone = _zone WHERE accountId = _id;
    END $$
DROP PROCEDURE IF EXISTS thevoid.update_account_level_up $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE update_account_level_up (_id INT, _level SMALLINT, _experience SMALLINT, _remainingPoints TINYINT)
	BEGIN
		UPDATE statistics SET statLevel = _level, statExperience = _experience, statRemainingPoints = _remainingPoints WHERE accountId = _id;
    END $$
DROP PROCEDURE IF EXISTS thevoid.update_account_torsoArmour $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE update_account_torsoArmour(_id INT, _itemId SMALLINT)
	BEGIN
		UPDATE inventory SET equipTorso = _itemId WHERE accountId = _id;
    END $$
DROP PROCEDURE IF EXISTS thevoid.update_account_headArmour $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE update_account_headArmour(_id INT, _itemId SMALLINT)
	BEGIN 
		UPDATE inventory SET equipHead = _itemId WHERE accountId = _id;
	END $$
DROP PROCEDURE IF EXISTS thevoid.update_account_legsArmour $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE update_account_legsArmour(_id INT, _itemId SMALLINT)
	BEGIN
		UPDATE inventory SET equipLegs = _itemId WHERE accountId = _id;
	END $$
DROP PROCEDURE IF EXISTS thevoid.update_account_shield $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE update_account_shield(_id INT, _itemId SMALLINT)
	BEGIN
		UPDATE inventory SET equipShield = _itemId WHERE accountId = _id;
	END $$
DROP PROCEDURE IF EXISTS thevoid.update_account_weapon $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE update_account_weapon(_id INT, _itemId SMALLINT)
	BEGIN
		UPDATE inventory SET equipWeapon = _itemId WHERE accountId = _id;
	END $$
DROP PROCEDURE IF EXISTS thevoid.update_account_inventory_slot $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE update_account_inventory_slot(_id INT, _slot VARCHAR(6), _itemId SMALLINT)
	BEGIN
			IF _slot = 'slot0' 
				THEN
					UPDATE inventory SET slot0 = _itemId WHERE accountId = _id;
			ELSEIF _slot = 'slot1' 
				THEN 
					UPDATE inventory SET slot1 = _itemId WHERE accountId = _id;
			ELSEIF _slot = 'slot2' 
				THEN 
					UPDATE inventory SET slot2 = _itemId WHERE accountId = _id;
			ELSEIF _slot = 'slot3' 
				THEN 
					UPDATE inventory SET slot3 = _itemId WHERE accountId = _id;
			ELSEIF _slot = 'slot4' 
				THEN 
					UPDATE inventory SET slot4 = _itemId WHERE accountId = _id;
			ELSEIF _slot = 'slot5' 
				THEN 
					UPDATE inventory SET slot5 = _itemId WHERE accountId = _id;
			ELSEIF _slot = 'slot6' 
				THEN 
					UPDATE inventory SET slot6 = _itemId WHERE accountId = _id;
			ELSEIF _slot = 'slot7' THEN 
				UPDATE inventory SET slot7 = _itemId WHERE accountId = _id;
		END IF;
    END $$
DROP PROCEDURE IF EXISTS thevoid.get_monster $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE get_monster (_id SMALLINT)
	BEGIN
		SELECT * FROM monster WHERE id = _id;
	END $$
DROP PROCEDURE IF EXISTS thevoid.get_monster_by_zone $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE get_monster_by_zone (_id SMALLINT)
	BEGIN
		SELECT id AS result FROM monster WHERE monstZone = _id ORDER BY RAND() LIMIT 1;
    END $$
DROP PROCEDURE IF EXISTS thevoid.get_monster_loot $$
CREATE DEFINER = 'main_admin'@'localhost' PROCEDURE get_monster_loot (_id SMALLINT)
	BEGIN
		SELECT * FROM monsterloot WHERE monsterId = _id;
	END $$
DELIMITER ;
-- -----------------------------------------------------------------------------
-- ITEMS
-- -----------------------------------------------------------------------------
INSERT IGNORE INTO item (type, name, description, fromValue, toValue) VALUES
('NULL', 'NULL', 'NULL', '0', '0'),
('Weapon', 'Stick', 'Just some random tree branch you found on the ground.', '0', '0'),
('Shield', 'Wooden Shield', 'This kind of wooden protection wont do much for you. Still, better than nothing.', '0', '0'),
('Weapon', 'Rusty Sword', 'An old sword, long past its best before date.', '0', '0'),
('Weapon', 'Farmers Pitchfork', 'This tool was meant to be used in farming rather than fighting. It does have some sharp bits though.', '0', '0'),
('HeadArmour', 'Some HeadArmour', 'Description', '0', '0'),
('LegsArmour', 'Some LegsArmour', 'Description', '0', '0'),
('TorsoArmour', 'Some TorsoArmour', 'Description', '0', '0');

-- -----------------------------------------------------------------------------
-- ZONES
-- -----------------------------------------------------------------------------
INSERT IGNORE INTO zone (name, description) VALUES 
('Forest', 'Trees. Lots of trees.'),
('Mountains', 'Lots of rocks. And boulders.'),
('Cave', 'A big hole.'),
('ShroomLand', 'Shroooms.'),
('Marsh', 'A cavern with lakes. Or something like that.'),
('Lake Cavern', 'Lakes with holes.'),
('Misty Ruins', 'Misty twisty.');

-- -----------------------------------------------------------------------------
-- MONSTERS
-- -----------------------------------------------------------------------------
INSERT IGNORE INTO monster (monstName, monstLevel, monstAttack, monstHitpoints, monstDefense, monstExperience, monstGold, monstZone, monstDescription) VALUES 
# FOREST ZONE
('Mosquito', '2', '8', '20', '10', '10', '5', '1', 'These blood-suckers may be weak, but they sure are annoying.'),
('Wolf', '4', '12', '20', '8', '12', '4', '1', 'The Big Bad.'),
('Thug', '6', '15', '22', '10', '20', '15', '1', 'Even in the void you get to meet outlaws. Can you even be an outlaw when there is no law?'),
('Ent', '5', '10', '23', '5', '15', '2', '1', 'Its a tree. A giant, moving tree that wants to kill you.'),
('Drop Bear', '6', '12', '20', '5', '15', '4', '1', 'Drop bears are known to live in the deep forests of the southern continent of Fourex. And apparently the void.'),

# MOUNTAINS ZONE 
('Yeti', '8', '20', '30', '10', '25', '7', '2', 'The Abominable Snowman.'),
('Mountain Goat', '8', '19', '28', '8', '24', '6', '2', 'Surprisingly sprightly. Better watch out!'),
('Snow Troll', '9', '22', '29', '12', '26', '5', '2', 'They are said to be native to the frozen northern wastelands of Angmar. Yet the depths of the void accept all…'),
('The Killer Rabbit of Carbannog', '10', '23', '31', '7', '28', '6', '2', 'Just a Harmless Little Bunny… right?'),
('Polar Bear', '11', '25', '32', '6', '30', '5', '2', 'A predator, hiding, waiting to hunt. you. down.'),

# CAVE ZONE
('Giant Spider', '12', '27', '34', '5', '33', '6', '3', 'Spiders... why did it have to be spiders...'),
('Goblin', '13', '28', '35', '5', '34', '18', '3', 'Trogy.'),
('Samodiva', '14', '30', '37', '4', '37', '25', '3', 'A living legend of eternal beauty and grace, the image of the ideal woman. Will eat your face.'),
('Troll', '15', '32', '40', '5', '39', '5', '23', 'Better not feed it.'),
('Lindworm', '16', '34', '39', '17', '42', '35', '3', 'The screams of the villagers it terrorized still echo in your ears...'),

# SHROOM LAND ZONE
('Blister Beetle', '17', '36', '41', '8', '44', '15', '4', 'At this size might cause a bit more than a blister.'),
('Shroom Man', '18', '37', '42', '12', '46', '42', '4', 'Nana nana nana nana Shroom Man!'),
('Shiny Mushroom Creature', '19', '39', '44', '6', '48', '22', '4', 'Probably not edible...'),
('Puffball Mushroom', '20', '41', '45', '7', '51', '34', '4', 'What.. even... is.. that....'),
('Mi-Go', '25', '43', '47', '25', '155', '50', '4', 'I saw—just once—that hollow of old oaks, Grey with a ground-mist that enfolds and chokes The slinking shapes which madness has defiled.'),

# MARSH ZONE
('Toad', '23', '46', '49', '11', '58', '45', '5', 'All hail the hallucinogenic toad!'),
('Ogre', '20', '49', '51', '11', '60', '60', '5', 'Why does it make you think of onions?'),
('Witch', '25', '50', '52', '9', '65', '64', '5', 'Weighs the same as a duck!'),
('Grootslang', '22', '52', '33', '12', '77', '70', '5', 'I AM GROOT...slang'),
('Giant Turtle', '27', '54', '48', '10', '70', '60', '5', 'You could almost believe this glorious creature could one day carry a whole world on its back. On the other hand, turtle soup is said to be exquisite!'),

# LAKE CAVERN ZONE
('Hydra', '30', '55', '57', '14', '70', '84', '6', 'Even the breath exhaled by the Hydra can be lethal to any man.'),
('Kelpie', '35', '40', '48', '12', '71', '90', '6', 'Likes to trick unsuspecting passersby and drown them...'),
('Nessie', '20', '59', '60', '20', '80', '95', '6', 'Rising up from the very depths of the lake, this beast is not just some fairytale fish…'),
('Golden Pike', '38', '61', '55', '9', '90', '100', '6', 'Might not grant you any wishes, but it sure does taste good with some lemon and herbs!'),
('Narwhal', '32', '63', '58', '13', '100', '98', '6', 'Hey, that looks kinda cute. Lets kill it!'),

# MISTY RUINS ZONE
('The Black Knight', '30', '65', '67', '14', '110', '105', '7', 'Once a proud protector of the realm, the long traverse through the void seems to have corrupted him.'),
('White Lady', '36', '80', '47', '13', '138', '118', '7', 'Oh, my children!'),
('Banshee', '40', '70', '51', '14', '159', '126', '7', 'Her scream predicts a death. Always.'),
('Knight Artorias', '34', '75', '103', '18', '147', '197', '7', 'Whatever thou art... Stay away!'),
('Hag of the Mist', '42', '62', '120', '20', '163', '203', '7', 'How now, you secret, black, and midnight hags? What is ’t you do?'); 

-- -----------------------------------------------------------------------------
-- MONSTER LOOT
-- -----------------------------------------------------------------------------
INSERT IGNORE INTO monsterLoot (monsterId, itemId, itemChance) VALUES
('3', '1', '80'),
('3', '2', '50'),
('5', '1', '10'),
('5', '2', '30'),
('5', '3', '50'),
('7', '1', '10'),
('7', '2', '5'),
('9', '1', '20'),
('9', '2', '25'),
('9', '3', '30');